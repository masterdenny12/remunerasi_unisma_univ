-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Waktu pembuatan: 28 Jun 2020 pada 14.25
-- Versi server: 5.7.26
-- Versi PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_remunerasi_unisma`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `d_pegawai`
--

DROP TABLE IF EXISTS `d_pegawai`;
CREATE TABLE IF NOT EXISTS `d_pegawai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nip` char(100) DEFAULT NULL,
  `kategori_pegawai_id` int(11) DEFAULT NULL,
  `nama_pegawai` varchar(50) DEFAULT NULL,
  `tanggal_lahir` varchar(50) DEFAULT NULL,
  `no_telp` varchar(50) DEFAULT NULL,
  `jenis_kelamin` char(30) DEFAULT NULL,
  `kota_id` int(11) DEFAULT NULL,
  `alamat` text,
  `foto` varchar(255) DEFAULT NULL,
  `status_perkawinan_id` int(11) DEFAULT NULL,
  `status_ikatan_kerja_id` int(11) DEFAULT NULL,
  `status_pegawai` enum('aktif','tidak aktif') DEFAULT NULL,
  `tgl_mulai_masuk` varchar(100) DEFAULT NULL,
  `pendidikan_terakhir_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `d_pegawai`
--

INSERT INTO `d_pegawai` (`id`, `nip`, `kategori_pegawai_id`, `nama_pegawai`, `tanggal_lahir`, `no_telp`, `jenis_kelamin`, `kota_id`, `alamat`, `foto`, `status_perkawinan_id`, `status_ikatan_kerja_id`, `status_pegawai`, `tgl_mulai_masuk`, `pendidikan_terakhir_id`, `created_at`, `updated_at`) VALUES
(15, '11111111', 1, 'billa yiha', '29-01-2019', '081111113', 'P', 2, 'mojokerto pacet', '11111111.png', 2, 2, 'aktif', NULL, 1, '2020-06-24 05:35:27', '2020-06-24 05:35:27'),
(13, '11222', 2, 'denny kharisma putra', '23-06-2020', '08918882323', 'P', 2, 'Magersari VII / 10 RT.002 RW.002', '11222.jpg', 2, 1, 'aktif', NULL, 2, '2020-06-21 01:13:56', '2020-06-22 13:44:15'),
(10, '12345', 3, 'hehe', '25-06-2020', '08918882323', 'L', 1, 'Dsn.Mojojejer  RT.03 RW.04 Ds.Pesanggrahan Kec.Kutorejo', NULL, 1, 1, 'tidak aktif', NULL, 3, '2020-06-20 05:40:08', '2020-06-22 13:44:17');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_jabatan`
--

DROP TABLE IF EXISTS `m_jabatan`;
CREATE TABLE IF NOT EXISTS `m_jabatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jabatan` varchar(100) NOT NULL DEFAULT '0',
  `keterangan` varchar(255) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_jabatan`
--

INSERT INTO `m_jabatan` (`id`, `jabatan`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 'Rektor', '', '2020-06-21 10:38:29', '2020-06-21 10:38:30'),
(2, 'Wakil Rektor ', '', '2020-06-21 10:39:52', '2020-06-21 10:39:53'),
(3, 'Dekan', '', '2020-06-21 10:39:52', '2020-06-21 10:39:53'),
(4, 'Wakil Dekan', '', '2020-06-21 10:39:52', '2020-06-21 10:39:53'),
(5, 'Kepala Bagian', '', '2020-06-21 10:39:52', '2020-06-21 10:39:53'),
(6, 'Kepala Subbagian', '', '2020-06-21 10:39:52', '2020-06-21 10:39:53'),
(7, 'Direktur', '', '2020-06-21 10:39:52', '2020-06-21 10:39:53'),
(8, 'Wakil Direktur', '', '2020-06-21 10:39:52', '2020-06-21 10:39:53'),
(9, 'Ketua', '', '2020-06-21 10:39:52', '2020-06-21 10:39:53'),
(10, 'Sekretaris', '', '2020-06-21 10:39:52', '2020-06-21 10:39:53');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_jenjang_pendidikan`
--

DROP TABLE IF EXISTS `m_jenjang_pendidikan`;
CREATE TABLE IF NOT EXISTS `m_jenjang_pendidikan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_pendidikan` varchar(100) NOT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_jenjang_pendidikan`
--

INSERT INTO `m_jenjang_pendidikan` (`id`, `nm_pendidikan`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 'S1', 'sarjana S1', '2020-06-21 02:31:04', '2020-06-21 02:31:04'),
(2, 'S2', 'Sarjana S2', '2020-06-21 02:31:11', '2020-06-21 02:31:11'),
(3, 'D4', 'Diploma 4', '2020-06-21 02:31:23', '2020-06-21 02:31:23'),
(4, 'D3', 'Diploma 3', '2020-06-21 02:31:28', '2020-06-21 02:31:28');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_kategori_pegawai`
--

DROP TABLE IF EXISTS `m_kategori_pegawai`;
CREATE TABLE IF NOT EXISTS `m_kategori_pegawai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_pegawai` varchar(100) NOT NULL DEFAULT '0',
  `keterangan` varchar(100) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_kategori_pegawai`
--

INSERT INTO `m_kategori_pegawai` (`id`, `jenis_pegawai`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 'Dosen', 'Pengajar', '2020-06-21 08:21:43', '2020-06-21 08:21:44'),
(2, 'Karyawan', 'Staff', '2020-06-21 08:21:50', '2020-06-21 08:21:51'),
(3, 'Pejabat', '0', '2020-06-21 08:21:50', '2020-06-21 08:21:51'),
(4, 'Security', '0', '2020-06-21 08:21:50', '2020-06-21 08:21:51'),
(5, 'Driver', '0', '2020-06-21 08:21:50', '2020-06-21 08:21:51'),
(6, 'Juru parkir', '0', '2020-06-21 08:21:50', '2020-06-21 08:21:51');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_kategori_penilaian_dosen`
--

DROP TABLE IF EXISTS `m_kategori_penilaian_dosen`;
CREATE TABLE IF NOT EXISTS `m_kategori_penilaian_dosen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(100) NOT NULL DEFAULT '0',
  `bobot` varchar(50) NOT NULL DEFAULT '0',
  `skor_target` varchar(50) NOT NULL DEFAULT '0',
  `keterangan` varchar(255) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_kategori_penilaian_dosen`
--

INSERT INTO `m_kategori_penilaian_dosen` (`id`, `kategori`, `bobot`, `skor_target`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 'Kinerja Akademik (ITIKAD)', '0.6', '25.17', 'Kategori A', '2020-06-27 05:49:21', '2020-06-27 05:49:21'),
(2, 'Perilaku (Pelaksanaan pekerjaan)', '0.2', '82.50', 'Kategori B', '2020-06-21 05:22:49', '2020-06-21 05:22:49'),
(3, 'Kedisiplinan pegawai', '0.2', '80.00', 'Kategori C', '2020-06-21 05:23:07', '2020-06-21 05:23:07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_komponen_kompetensi_dosen`
--

DROP TABLE IF EXISTS `m_komponen_kompetensi_dosen`;
CREATE TABLE IF NOT EXISTS `m_komponen_kompetensi_dosen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_kategori_dosen_id` int(11) NOT NULL DEFAULT '0',
  `komponen_kompetensi` varchar(255) NOT NULL DEFAULT '0',
  `deskripsi_skor1` varchar(255) NOT NULL DEFAULT '0',
  `deskripsi_skor2` varchar(255) NOT NULL DEFAULT '0',
  `deskripsi_skor3` varchar(255) NOT NULL DEFAULT '0',
  `deskripsi_skor4` varchar(255) NOT NULL DEFAULT '0',
  `deskripsi_skor5` varchar(255) NOT NULL DEFAULT '0',
  `bukti_pendukung` varchar(255) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_komponen_kompetensi_dosen`
--

INSERT INTO `m_komponen_kompetensi_dosen` (`id`, `sub_kategori_dosen_id`, `komponen_kompetensi`, `deskripsi_skor1`, `deskripsi_skor2`, `deskripsi_skor3`, `deskripsi_skor4`, `deskripsi_skor5`, `bukti_pendukung`, `created_at`, `updated_at`) VALUES
(1, 1, 'Nilai Rerata evaluasi perkuliahan untuk semester gasal- sem genap', 'Nilai rerata < 10% (KURANG)', 'Nilai rerata>10% - < 25% (CUKUP)', 'Nilai rerata>=26% - < 50% (BAIK)', 'Nilai rerata>=51% - < 75% (SANGAT BAIK)', 'Nilai rerata>=75% - - 100% (ISTIMEWA)', 'Bukti hasil evaluasi perkuliahan', '2020-06-22 09:23:50', '2020-06-22 09:23:50'),
(2, 1, 'Dosen Menyusun RPS dari setiap mata kuliah yang diasuhnya dalam satu tahun akademik', 'tidak menyusun sama sekali', 'menyusun kurang dari 25% untukmata kuliah yang diasuhnya', 'Nilai rerata>=26% - < 50% (BAIK)', 'Nilai rerata>=51% - < 75% (SANGAT BAIK)', 'Nilai rerata>=75% - - 100% (ISTIMEWA)', 'checklist RPS dari sekretariat fakultas', '2020-06-22 09:23:50', '2020-06-22 09:23:50'),
(3, 3, 'Dapat meiliki rancangan', 'tidak sama sekali', 'menyusun kurang dari 25% untukmata kuliah yang diasuhnya', 'Nilai rerata>=26% - < 50% (BAIK)', 'Nilai rerata>=51% - < 75% (SANGAT BAIK)', 'Nilai rerata>=75% - - 100% (ISTIMEWA)', 'checklist RPS dari sekretariat fakultas', '2020-06-22 09:23:50', '2020-06-22 09:23:50');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_kota`
--

DROP TABLE IF EXISTS `m_kota`;
CREATE TABLE IF NOT EXISTS `m_kota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kota` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_kota`
--

INSERT INTO `m_kota` (`id`, `nama_kota`, `created_at`, `updated_at`) VALUES
(1, 'Malang', '2020-06-20 11:16:55', '2020-06-20 11:16:56'),
(2, 'Surabaya', '2020-06-21 06:07:57', '2020-06-21 06:07:57');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_nama_sekolah`
--

DROP TABLE IF EXISTS `m_nama_sekolah`;
CREATE TABLE IF NOT EXISTS `m_nama_sekolah` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_sekolah` varchar(100) NOT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_nama_sekolah`
--

INSERT INTO `m_nama_sekolah` (`id`, `nama_sekolah`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 'Unisma', 'Universitas Islam Malang', '2020-06-21 02:51:01', '2020-06-21 02:51:01'),
(2, 'Universitas Brawijaya', 'UB', '2020-06-21 05:24:22', '2020-06-21 05:24:22'),
(5, 'politeknik negeri malang', 'polinema', '2020-06-21 02:52:54', '2020-06-21 09:52:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_status_ikatan_kerja`
--

DROP TABLE IF EXISTS `m_status_ikatan_kerja`;
CREATE TABLE IF NOT EXISTS `m_status_ikatan_kerja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ikatan_kerja` varchar(255) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_status_ikatan_kerja`
--

INSERT INTO `m_status_ikatan_kerja` (`id`, `ikatan_kerja`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 'Permanen', '', '2020-06-20 11:39:59', '2020-06-20 11:39:59'),
(2, 'Kontrak', 'kontrak kerja', '2020-06-21 02:08:47', '2020-06-21 02:08:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_status_perkawinan`
--

DROP TABLE IF EXISTS `m_status_perkawinan`;
CREATE TABLE IF NOT EXISTS `m_status_perkawinan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_perkawinan` varchar(100) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_status_perkawinan`
--

INSERT INTO `m_status_perkawinan` (`id`, `status_perkawinan`, `created_at`, `updated_at`) VALUES
(1, 'Menikah', '2020-06-20 11:46:53', '2020-06-20 11:46:53'),
(2, 'Belum Menikah', '2020-06-20 11:47:05', '2020-06-20 11:47:05');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_sub_kategori_penilaian_dosen`
--

DROP TABLE IF EXISTS `m_sub_kategori_penilaian_dosen`;
CREATE TABLE IF NOT EXISTS `m_sub_kategori_penilaian_dosen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_penilaian_id` int(11) NOT NULL DEFAULT '0',
  `sub_kategori` varchar(255) NOT NULL DEFAULT '0',
  `skor_target` varchar(255) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_sub_kategori_penilaian_dosen`
--

INSERT INTO `m_sub_kategori_penilaian_dosen` (`id`, `kategori_penilaian_id`, `sub_kategori`, `skor_target`, `created_at`, `updated_at`) VALUES
(1, 1, 'Pendidikan dan Pengajaran', '16.03', '2020-06-22 04:34:26', '2020-06-22 04:34:26'),
(2, 1, 'Penelitian dan Karya Ilmiah', '5.22', '2020-06-22 04:34:26', '2020-06-22 04:34:26'),
(3, 2, 'Kesetian', '80.00', '2020-06-22 04:48:29', '2020-06-22 04:48:29'),
(4, 1, 'Pengabdian Kepada Masyarakat', '1.84', '2020-06-22 04:34:26', '2020-06-22 04:34:26'),
(5, 1, 'Unsur Penunjang Pengapdian kepada Universitas, dan Pengembangan diri', '2.08', '2020-06-22 04:34:26', '2020-06-22 04:34:26'),
(9, 2, 'Prestasi kerja', '80.0', '2020-06-22 04:34:26', '2020-06-22 04:34:26'),
(10, 2, 'Tanggung Jawab', '80.0', '2020-06-22 04:34:26', '2020-06-22 04:34:26'),
(11, 2, 'Ketaatan', '80.0', '2020-06-22 04:34:26', '2020-06-22 04:34:26'),
(12, 2, 'Kejujuran', '100.0', '2020-06-22 04:34:26', '2020-06-22 04:34:26'),
(13, 2, 'Kerjasama', '80.0', '2020-06-22 04:34:26', '2020-06-22 04:34:26'),
(14, 2, 'Prakarsa', '80.0', '2020-06-22 04:34:26', '2020-06-22 04:34:26'),
(15, 2, 'Kepemimpinan', '80.0', '2020-06-22 04:34:26', '2020-06-22 04:34:26'),
(16, 3, 'Kedisiplinan Akademik', '80.0', '2020-06-22 04:34:26', '2020-06-22 04:34:26'),
(17, 3, 'Kedisiplinan Kepegawaian', '80.0', '2020-06-22 04:34:26', '2020-06-22 04:34:26');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_unit_kerja`
--

DROP TABLE IF EXISTS `m_unit_kerja`;
CREATE TABLE IF NOT EXISTS `m_unit_kerja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(50) NOT NULL DEFAULT '0',
  `unit_kerja` varchar(255) NOT NULL DEFAULT '0',
  `keterangan` varchar(255) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_unit_kerja`
--

INSERT INTO `m_unit_kerja` (`id`, `kode`, `unit_kerja`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, '1', 'Unit Penjaminan Mutu Akademik', 'Akademik', '2020-06-21 10:57:14', '2020-06-21 10:57:14'),
(2, '2', 'Unit Humas dan Protokol', 'Humas', '2020-06-21 10:57:29', '2020-06-21 10:57:29'),
(3, '3', 'Internal Audit dan Kelembagaan', 'Kelembagaan', '2020-06-21 10:57:52', '2020-06-21 10:57:52'),
(4, '4', 'Unit Pengamanan', 'Pengamanan', '2020-06-21 10:58:15', '2020-06-21 10:58:15'),
(5, '5', 'Unit Kearsipan dan Dokumentasi', 'Dokumentasi', '2020-06-21 10:58:47', '2020-06-21 10:58:47'),
(6, '6', 'Unit Pengadaan Barang dan Jasa', 'Pengadaan', '2020-06-21 10:58:57', '2020-06-21 10:58:57');

-- --------------------------------------------------------

--
-- Struktur dari tabel `p_penilaian_pegawai_dosen`
--

DROP TABLE IF EXISTS `p_penilaian_pegawai_dosen`;
CREATE TABLE IF NOT EXISTS `p_penilaian_pegawai_dosen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pegawai_id` varchar(50) DEFAULT NULL,
  `kategori_penilaian_dosen_id` int(11) DEFAULT NULL,
  `sub_kategori_penilaian_dosen_id` int(11) DEFAULT NULL,
  `skor_capaian` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `p_penilaian_pegawai_dosen`
--

INSERT INTO `p_penilaian_pegawai_dosen` (`id`, `pegawai_id`, `kategori_penilaian_dosen_id`, `sub_kategori_penilaian_dosen_id`, `skor_capaian`, `created_at`, `updated_at`) VALUES
(1, '11222', 1, 1, '23.28', '2020-06-27 08:05:48', '2020-06-28 10:36:38'),
(2, '11222', 1, 2, '20.48', '2020-06-27 08:05:48', '2020-06-28 10:36:41'),
(3, '11222', 2, 3, '80.00', '2020-06-27 08:05:48', '2020-06-28 10:36:42'),
(4, '11222', 1, 4, '6.16', '2020-06-27 08:05:48', '2020-06-28 10:36:55'),
(5, '11222', 1, 5, '5.63', '2020-06-27 08:05:48', '2020-06-28 10:36:57'),
(6, '11222', 3, 16, '90.00', '2020-06-27 08:05:48', '2020-06-28 10:36:57'),
(7, '11222', 3, 17, '100.00', '2020-06-27 08:05:48', '2020-06-28 10:36:58');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `foto`, `email_verified_at`, `password`, `role_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'admin', 'admin@gmail.com', '', NULL, '$2y$10$1wbZWyI2Ryj.j2Pt4yA2fOSZUZrxu6WqQ2ehr8Cu9vZmo4BhxvsSm', 1, NULL, '2020-06-19 16:15:18', '2020-06-19 16:15:19');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
