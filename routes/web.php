<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'LoginController@index')->name('login');
Route::post('post-login', 'LoginController@postLogin');
Route::match(['post', 'get'],'reg', 'LoginController@postRegister');

Route::group(['middleware' => ['auth']], function(){
Route::get('dashboard', ['uses' => 'DashboardController@index']);
//data pegawai
Route::get('data-pegawai', ['uses' => 'PegawaiController@index']);
Route::get('datatable_pegawai', ['uses' => 'PegawaiController@show_data']);
Route::match(['post','get'],'simpan_data_pegawai', 'PegawaiController@store');
Route::match(['post','get'],'update_data_pegawai', 'PegawaiController@update');
Route::post('destroy_data_pegawai', ['uses' => 'PegawaiController@destroy']);
Route::get('ajax/hris/emp/detail/{emp_id}', 'PegawaiController@AjaxDetailEmployee')->name('ajax-hris-detail-employee');

//master ikatan kerja
Route::get('master-ikatan-kerja', ['uses' => 'IkatanKerjaController@index']);
Route::get('datatable_ikatan_kerja', ['uses' => 'IkatanKerjaController@show_data']);
Route::match(['post','get'],'simpan_master_ikatan_kerja', 'IkatanKerjaController@store');
Route::match(['post','get'],'update_master_ikatan_kerja', 'IkatanKerjaController@update');
Route::post('destroy_master_ikatan_kerja', ['uses' => 'IkatanKerjaController@destroy']);

//master jenjang pendidikan
Route::get('master-jenjang-pendidikan', ['uses' => 'JenjangPendidikanController@index']);
Route::get('datatable_jenjang_pendidikan', ['uses' => 'JenjangPendidikanController@show_data']);
Route::match(['post','get'],'simpan_master_jenjang_pendidikan', 'JenjangPendidikanController@store');
Route::match(['post','get'],'update_master_jenjang_pendidikan', 'JenjangPendidikanController@update');
Route::post('destroy_master_jenjang_pendidikan', ['uses' => 'JenjangPendidikanController@destroy']);

//master jenjang pendidikan
Route::get('master-nama-sekolah', ['uses' => 'NamaSekolahController@index']);
Route::get('datatable_nama_sekolah', ['uses' => 'NamaSekolahController@show_data']);
Route::match(['post','get'],'simpan_master_nama_sekolah', 'NamaSekolahController@store');
Route::match(['post','get'],'update_master_nama_sekolah', 'NamaSekolahController@update');
Route::post('destroy_master_nama_sekolah', ['uses' => 'NamaSekolahController@destroy']);

//master kategori penilaian dosen
Route::get('master-kategori-penilaian', ['uses' => 'KategoriPenilaianController@index']);
Route::get('datatable_kategori_penilaian', ['uses' => 'KategoriPenilaianController@show_data']);
Route::match(['post','get'],'simpan_master_kategori_penilaian', 'KategoriPenilaianController@store');
Route::match(['post','get'],'update_master_kategori_penilaian', 'KategoriPenilaianController@update');
Route::post('destroy_master_kategori_penilaian', ['uses' => 'KategoriPenilaianController@destroy']);

//master kategori penilaian dosen
Route::get('datatable_sub_kategori_penilaian/{id_kategori}', ['uses' => 'SubKategoriPenilaianDosenController@show_kategori']);
Route::get('show_sub_kategori_dosen/{id_kategori}', ['uses' => 'KategoriPenilaianController@show_sub_kategori_dosen']);
Route::get('datatable_komponen_kompetensi/{id_sub_kategori}', ['uses' => 'KategoriPenilaianController@show_komponen_kompetensi']);
Route::match(['post','get'],'simpan_master_kategori_penilaian', 'KategoriPenilaianController@store');
Route::match(['post','get'],'update_master_kategori_penilaian', 'KategoriPenilaianController@update');
Route::post('destroy_master_kategori_penilaian', ['uses' => 'KategoriPenilaianController@destroy']);

//select
Route::get('/cari/ikatan_kerja', ['uses' => 'SelectController@loadIkatanKerja']);
Route::get('/cari/status_perkawinan', ['uses' => 'SelectController@loadStatusPerkawinan']);
Route::get('/cari/kota', ['uses' => 'SelectController@loadKotas']);
Route::get('/cari/jenjang_pendidikan', ['uses' => 'SelectController@loadJenjangPendidikan']);
Route::get('/cari/jenis_pegawai', ['uses' => 'SelectController@loadKategoriPegawai']);
Route::get('/cari/nama_pegawai', ['uses' => 'SelectController@loadNamaPegawai']);

//profile
Route::get('profile', ['uses' => 'ProfileController@index']);
Route::post('editPassword', 'ProfileController@editPassword');

//logout
Route::get('logout', 'LoginController@logout');

//Evaluasi pegawao
Route::get('evaluasi-pegawai', ['uses' => 'EvaluasiPegawaiController@index']);
Route::get('check_nip/{nip}', 'EvaluasiPegawaiController@check_nip');
Route::get('sub_kategori_penilaian_dosen/{id_kompetensi}', 'EvaluasiPegawaiController@sub_kategori_penilaian_dosen');
Route::get('hasil_evaluasi_penilaian_dosen/{nip}', 'EvaluasiPegawaiController@hasil_evaluasi_penilaian_dosen');

//sub kategori penilaian dosen
Route::post('simpan_sub_kategori_penilaian_dosen', ['uses' => 'SubKategoriPenilaianDosenController@store']);
Route::post('update_sub_kategori_penilian_dosen', ['uses' => 'SubKategoriPenilaianDosenController@update']);
Route::post('destroy_sub_kategori_penilian_dosen', ['uses' => 'SubKategoriPenilaianDosenController@destroy']);
});
