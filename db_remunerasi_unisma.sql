-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Waktu pembuatan: 20 Jun 2020 pada 07.35
-- Versi server: 5.7.26
-- Versi PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_remunerasi_unisma`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `d_pegawai`
--

DROP TABLE IF EXISTS `d_pegawai`;
CREATE TABLE IF NOT EXISTS `d_pegawai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nip` char(100) DEFAULT NULL,
  `nama_pegawai` varchar(50) DEFAULT NULL,
  `tanggal_lahir` varchar(50) DEFAULT NULL,
  `jenis_kelamin` char(30) DEFAULT NULL,
  `kota_id` int(11) DEFAULT NULL,
  `alamat` text,
  `foto` varchar(255) DEFAULT NULL,
  `status_perkawinan_id` int(11) DEFAULT NULL,
  `status_ikatan_kerja_id` int(11) DEFAULT NULL,
  `status_pegawai` enum('aktif','tidak aktif') DEFAULT NULL,
  `tgl_mulai_masuk` varchar(100) DEFAULT NULL,
  `pendidikan_terakhir_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `d_pegawai`
--

INSERT INTO `d_pegawai` (`id`, `nip`, `nama_pegawai`, `tanggal_lahir`, `jenis_kelamin`, `kota_id`, `alamat`, `foto`, `status_perkawinan_id`, `status_ikatan_kerja_id`, `status_pegawai`, `tgl_mulai_masuk`, `pendidikan_terakhir_id`, `created_at`, `updated_at`) VALUES
(2, '12345678', 'putra k putra', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-19 23:04:44', '2020-06-19 23:04:44'),
(3, '11122344', 'rofi imron', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-19 21:45:04', '2020-06-20 04:45:04');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_jenjang_pendidikan`
--

DROP TABLE IF EXISTS `m_jenjang_pendidikan`;
CREATE TABLE IF NOT EXISTS `m_jenjang_pendidikan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_pendidikan` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_kota`
--

DROP TABLE IF EXISTS `m_kota`;
CREATE TABLE IF NOT EXISTS `m_kota` (
  `id` int(11) DEFAULT NULL,
  `nama_kota` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_nama_sekolah`
--

DROP TABLE IF EXISTS `m_nama_sekolah`;
CREATE TABLE IF NOT EXISTS `m_nama_sekolah` (
  `id` int(11) DEFAULT NULL,
  `nama_sekolah` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_status_ikatan_kerja`
--

DROP TABLE IF EXISTS `m_status_ikatan_kerja`;
CREATE TABLE IF NOT EXISTS `m_status_ikatan_kerja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ikatan_kerja` varchar(255) NOT NULL DEFAULT '0',
  `created_at` varchar(255) NOT NULL DEFAULT '0',
  `updated_at` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_status_perkawinan`
--

DROP TABLE IF EXISTS `m_status_perkawinan`;
CREATE TABLE IF NOT EXISTS `m_status_perkawinan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_perkawinan` varchar(100) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `email_verified_at`, `password`, `role_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'admin', 'admin@gmail.com', NULL, '$2y$10$QQHS5QD8VdNTxpqIYI2hyuxjiRNwCAIxWGsziz3HL9XART1ekLgxG', 1, NULL, '2020-06-19 16:15:18', '2020-06-19 16:15:19');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
