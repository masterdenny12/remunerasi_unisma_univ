<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use Illuminate\Support\Carbon;

class IkatanKerjaController extends Controller
{
    public function index(){

        // dd($queryy);
        return view('ikatankerja.index');
    }

    public function show_data(){
        try {
            $result = [];
            $count = 1;

                $query = \DB::table('m_status_ikatan_kerja')
                    ->select('*')
                    ->get();

            foreach ($query as $ikatankerja) {
                $action_edit = '<center><a href="#" class="btn btn-success btn-sm m-btn  m-btn m-btn--icon" id="btn-edit-ikatan-kerja"
                                data-id="' . $ikatankerja->id . '"
                                data-ikatan_kerja="' . $ikatankerja->ikatan_kerja . '"
                                data-keterangan="' . $ikatankerja->keterangan . '"
                                data-toggle="modal"
                                data-target="#modal-edit-ikatan-kerja">
                                <span>
                                    <i class="la la-archive"></i>
                                    <span>Update</span>
                                </span>
                                </a>';


                $action_del = '<a href="#" class="btn btn-danger m-btn btn-sm m-btn m-btn--icon" id="btn-delete-ikatan-kerja"
                                data-id="' . $ikatankerja->id . '">
                                <span>
                                    <i class="la la-warning"></i>
                                    <span>Delete</span>
                                </span>
                                </a></center>';

                $update = $ikatankerja->updated_at ? \Carbon\Carbon::parse($ikatankerja->updated_at)->format('d-m-Y H:i') : '';
                $data = [];
                $data[] = $count++;
                $data[] = strtoupper($ikatankerja->ikatan_kerja);
                $data[] = $update;
                $data[] = $action_edit.' '.$action_del;
                $result[] = $data;
            }
            return response()->json(['result' => $result]);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 406);
        }
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'ikatan_kerja' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }
        $date = Carbon::now();

        DB::table('m_status_ikatan_kerja')
                ->insert(['ikatan_kerja' => $request->ikatan_kerja,
                            'keterangan'  => $request->keterangan,
                            'created_at'    => $date
                ]);

        return response()->json(['success'=>'Data berhasil ditambahkan']);
    }

    public function update(Request $request)
    {
        $rules = [
            'id' => 'required',
            'ikatan_kerja' => 'required',

        ];
        $messages = [
            'required' => 'The :attribute is required.',
            'min' => 'The :attribute is lest than 3 character.',
        ];
        //validation roles
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->all()], 406);
        }

        try {
            \DB::table('m_status_ikatan_kerja')->where('id', $request->id)->update([
                'ikatan_kerja' => $request->ikatan_kerja,
                'keterangan' => $request->keterangan,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
            return response()->json(['status' => 'success', 'result' => 'Data berhasil diubah'], 200);
        } catch (\Exception $exception) {
            return response()->json(['status' => 'error', 'message' => $exception->getMessage()], 406);
        }
    }

    public function destroy(Request $request)
    {
        try {
            \DB::table('m_status_ikatan_kerja')->where('id', '=', $request->id)->delete();

        } catch (Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 404);
        }
        return response()->json(['status' => 'success', 'result' => 'Data berhasil dihapus'], 200);
    }

}
