<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Exception;

class EvaluasiPegawaiController extends Controller
{
    public function index(Request $request)
    {

        $post = $request->nip;
        return view('evaluasipegawai.index', compact(
                                                    'hasil_evaluasi_penilaian_dosen',
                                                    'post'

                                            ));
    }

    public function hasil_evaluasi_penilaian_dosen($nip){

        // dd($query_check_nip);
        $query = DB::table('m_kategori_penilaian_dosen as kpd')
                ->select('kpd.id as id_kategori_penilaian_dosen','kpd.kategori', 'kpd.bobot','kpd.skor_target')
                ->get();
        // dd($query);
        $arr = [];
        foreach ($query as $key => $value){
            $query_kompetensi = DB::table('m_sub_kategori_penilaian_dosen as kp')
                                ->Leftjoin('m_kategori_penilaian_dosen as kpd', 'kp.kategori_penilaian_id', 'kpd.id')
                                ->select('kp.kategori_penilaian_id','kp.sub_kategori','kp.skor_target', 'kp.id as id_sub_kategori_dosen')
                                ->where('kategori_penilaian_id', $value->id_kategori_penilaian_dosen)
                                ->get();

            $query_sum_kategori = DB::table('m_sub_kategori_penilaian_dosen as kp')
                                ->Leftjoin('m_kategori_penilaian_dosen as kpd', 'kp.kategori_penilaian_id', 'kpd.id')
                                ->select('kp.kategori_penilaian_id','kp.sub_kategori','kp.skor_target')
                                ->where('kategori_penilaian_id', $value->id_kategori_penilaian_dosen)
                                ->sum('kp.skor_target');

            $query_count = DB::table('m_sub_kategori_penilaian_dosen as kp')
                                ->Leftjoin('m_kategori_penilaian_dosen as kpd', 'kp.kategori_penilaian_id', 'kpd.id')
                                ->select('kp.kategori_penilaian_id','kp.sub_kategori','kp.skor_target')
                                ->where('kategori_penilaian_id', $value->id_kategori_penilaian_dosen)
                                ->count('kp.skor_target');


            // dd($query_penilaian);

        $push_kompetensi = [];
        foreach ($query_kompetensi as $key => $value_query_kompetensi){
            $query_penilaian = DB::table('p_penilaian_pegawai_dosen as ppd')
            ->Leftjoin('m_kategori_penilaian_dosen as kpd', 'ppd.kategori_penilaian_dosen_id', 'kpd.id')
            ->where('kategori_penilaian_dosen_id', $value->id_kategori_penilaian_dosen)
            ->where('sub_kategori_penilaian_dosen_id', $value_query_kompetensi->id_sub_kategori_dosen)
            ->where('pegawai_id', $nip)
            ->select('*')
            ->get();

            $query_sum_penilaian = DB::table('p_penilaian_pegawai_dosen as ppd')
            ->Leftjoin('m_kategori_penilaian_dosen as kpd', 'ppd.kategori_penilaian_dosen_id', 'kpd.id')
            ->where('kategori_penilaian_dosen_id', $value->id_kategori_penilaian_dosen)
            ->where('pegawai_id', $nip)
            ->select('*')
            ->sum('skor_capaian');


        $var_nilai  = 0;
        foreach ($query_penilaian as $key => $value_penilaian_pegawai) {
            $var_nilai = $value_penilaian_pegawai->skor_capaian;

        }
        $presentase_kategori_penilaian = ($var_nilai / $value_query_kompetensi->skor_target) * 100;
        $predikat = null;
        if($presentase_kategori_penilaian >= 120){
            $predikat = "ISTIMEWA";
        }else if($presentase_kategori_penilaian >= 110 && $presentase_kategori_penilaian < 120){
            $predikat = "SANGAT BAIK";
        }else if($presentase_kategori_penilaian >= 100 && $presentase_kategori_penilaian < 110){
            $predikat = "BAIK";
        }else if($presentase_kategori_penilaian >= 80 && $presentase_kategori_penilaian < 100){
            $predikat = "CUKUP";
        }else if($presentase_kategori_penilaian <= 80){
            $predikat = "KURANG";
        }

            array_push($push_kompetensi, ['kategori_penilaian_dosen'    => $value_query_kompetensi->kategori_penilaian_id,
                                          'id_sub_kategori_dosen'       => $value_query_kompetensi->id_sub_kategori_dosen,
                                          'sub_kategori_penilaian'      => $value_query_kompetensi->sub_kategori,
                                          'skor_target'                 => $value_query_kompetensi->skor_target,
                                          'skor_capaian'                => $var_nilai,
                                          'presentase_capaian'          => number_format($presentase_kategori_penilaian,2),
                                          'predikat'                    => $var_nilai == 0 ? '-' : $predikat,
                                          'check_available_penilaian'   => $var_nilai == 0 ? 'Nilai' : 'Nilai ulang',
                                          'button_color_check_nilai'    => $var_nilai == 0 ? 'warning' : 'danger',

            ]);
        }

        $count  =  $query_sum_kategori / $query_count;
        $count_total_penilaian_jumlah_bagi = $query_sum_penilaian / $query_count;
        $presentase_capaian_jumlah = $query_sum_penilaian / $query_sum_kategori * 100;
        $presentase_capaian_jumlah_bagi = $query_sum_penilaian / $count * 100;
        $predikat_total = null;
        if($presentase_capaian_jumlah >= 120){
            $predikat_total = "ISTIMEWA";
        }else if($presentase_capaian_jumlah >= 110 && $presentase_capaian_jumlah < 120){
            $predikat_total = "SANGAT BAIK";
        }else if($presentase_capaian_jumlah >= 100 && $presentase_capaian_jumlah < 110){
            $predikat_total = "BAIK";
        }else if($presentase_capaian_jumlah >= 80 && $presentase_capaian_jumlah < 100){
            $predikat_total = "CUKUP";
        }else if($presentase_capaian_jumlah <= 80){
            $predikat_total = "KURANG";
        }
        $nilai_capaian = $value->bobot * $presentase_capaian_jumlah;
        array_push($arr, ['kategori'                => $value->kategori,
                          'bobot'                   => $value->bobot,
                          'total_kategori'          => $query_count,
                          'skor_target_check'       => $value->id_kategori_penilaian_dosen ==  1 ? $query_sum_kategori : $count,
                          'skor_total_capaian'      => $value->id_kategori_penilaian_dosen ==  1 ? $query_sum_penilaian : $count_total_penilaian_jumlah_bagi,
                          'presentase_capaian'      => number_format($presentase_capaian_jumlah, 2),
                          'predikat_total'          => $predikat_total,
                          'nilai_capaian'           => number_format($nilai_capaian,2),
                          'sub_kategori_penilaian'  => $push_kompetensi
        ]);
        }
        $check_nip = DB::table('d_pegawai as p')
        ->Leftjoin('m_kategori_pegawai as kp', 'p.kategori_pegawai_id', 'kp.id')
        ->select(DB::raw('p.id, p.nip, p.nama_pegawai, kp.jenis_pegawai'))
        ->where('p.nip', $nip)
        ->first();
        // dd($arr);
        $id_kompetensi = 3;
        $sub_kategori_penilaian_dosen = $this->sub_kategori_penilaian_dosen($id_kompetensi);
        return view('evaluasipegawai.hasil_pegawai_dosen', compact('arr','check_nip', 'sub_kategori_penilaian_dosen'));


        // return $query;
    }

    public function check_nip($nip){
        $check_nip = DB::table('d_pegawai as p')
        ->Leftjoin('m_kategori_pegawai as kp', 'p.kategori_pegawai_id', 'kp.id')
        ->select(DB::raw('p.id, p.nip, p.nama_pegawai, kp.jenis_pegawai'))
        ->where('p.nip', $nip)
        ->first();
        echo json_encode($check_nip);
    }

    public function sub_kategori_penilaian_dosen($id_kompetensi){
        $query = DB::table('m_sub_kategori_penilaian_dosen')
                ->leftJoin('m_kategori_penilaian_dosen as kpd', 'm_sub_kategori_penilaian_dosen.kategori_penilaian_id', 'kpd.id')
                ->select('m_sub_kategori_penilaian_dosen.id as id_kategori_penilaian_dosen',
                 'kpd.bobot as bobot', 'm_sub_kategori_penilaian_dosen.sub_kategori')
                 ->where('m_sub_kategori_penilaian_dosen.id',$id_kompetensi)
                ->get();
        // dd($query);
        $arr = [];
        foreach ($query as $key => $value){
            $query_kompetensi = DB::table('m_komponen_kompetensi_dosen as kk')
            ->leftJoin('m_sub_kategori_penilaian_dosen as kp', 'kk.sub_kategori_dosen_id', 'kp.id')
            ->select('kk.komponen_kompetensi',
                     'kk.deskripsi_skor1',
                     'kk.deskripsi_skor2',
                     'kk.deskripsi_skor3',
                     'kk.deskripsi_skor4',
                     'kk.deskripsi_skor5',
                     'kk.bukti_pendukung',
                     'kk.id as id_komponen_kompetensi'
            )
            ->where('sub_kategori_dosen_id', $value->id_kategori_penilaian_dosen)
            ->get();
        $push_kompetensi = [];
        foreach ($query_kompetensi as $key => $value_query_kompetensi){
            array_push($push_kompetensi, ['id_komponen_kompetensi' => $value_query_kompetensi->id_komponen_kompetensi,
                                          'judul_komponen_kompetensi' => $value_query_kompetensi->komponen_kompetensi,
                                          'skor_1' => $value_query_kompetensi->deskripsi_skor1,
                                          'skor_2' => $value_query_kompetensi->deskripsi_skor2,
                                          'skor_3' => $value_query_kompetensi->deskripsi_skor3,
                                          'skor_4' => $value_query_kompetensi->deskripsi_skor4,
                                          'skor_5' => $value_query_kompetensi->deskripsi_skor5,
                                          'bukti_pendukung' => $value_query_kompetensi->bukti_pendukung,
                                          'bobot'   => $value->bobot
            ]);
        }
        array_push($arr, ['id_kategori_sub'     => $value->id_kategori_penilaian_dosen,
                          'sub_kategori'        => $value->sub_kategori,
                          'komponen_kompetensi' => $push_kompetensi
        ]);
        }
        // dd($arr);
        return json_encode($arr);


        // return $query;
    }
}
