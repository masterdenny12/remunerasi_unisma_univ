<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SelectController extends Controller
{
    public function loadKotas(Request $request)
    {
        if ($request->has('q')) {
            $cari = $request->q;
            $data =DB::table('m_kota')->select('id', 'nama_kota')->where('nama_kota', 'LIKE', '%'.$cari.'%')->get();
            return response()->json($data);
        }else{
            $data = DB::table('m_kota')->select('id', 'nama_kota')->get();
            return response()->json($data);
        }
    }

    public function loadIkatanKerja(Request $request)
    {
        if ($request->has('q')) {
            $cari = $request->q;
            $data =DB::table('m_status_ikatan_kerja')->select('id', 'ikatan_kerja')->where('ikatan_kerja', 'LIKE', '%'.$cari.'%')->get();
            return response()->json($data);
        }else{
            $data = DB::table('m_status_ikatan_kerja')->select('id', 'ikatan_kerja')->get();
            return response()->json($data);
        }
    }

    public function loadStatusPerkawinan(Request $request)
    {
        if ($request->has('q')) {
            $cari = $request->q;
            $data =DB::table('m_status_perkawinan')->select('id', 'status_perkawinan')->where('status_perkawinan', 'LIKE', '%'.$cari.'%')->get();
            return response()->json($data);
        }else{
            $data = DB::table('m_status_perkawinan')->select('id', 'status_perkawinan')->get();
            return response()->json($data);
        }
    }

    public function loadJenjangPendidikan(Request $request)
    {
        if ($request->has('q')) {
            $cari = $request->q;
            $data =DB::table('m_jenjang_pendidikan')->select('id', 'nm_pendidikan')->where('nm_pendidikan', 'LIKE', '%'.$cari.'%')->get();
            return response()->json($data);
        }else{
            $data = DB::table('m_jenjang_pendidikan')->select('id', 'nm_pendidikan')->get();
            return response()->json($data);
        }
    }

    public function loadKategoriPegawai(Request $request)
    {
        if ($request->has('q')) {
            $cari = $request->q;
            $data =DB::table('m_kategori_pegawai')->select('id', 'jenis_pegawai')->where('jenis_pegawai', 'LIKE', '%'.$cari.'%')->get();
            return response()->json($data);
        }else{
            $data = DB::table('m_kategori_pegawai')->select('id', 'jenis_pegawai')->get();
            return response()->json($data);
        }
    }
    public function loadNamaPegawai(Request $request)
    {
        if ($request->has('q')) {
            $cari = $request->q;
            $data =DB::table('d_pegawai')->select('id', 'nama_pegawai')->where('nama_pegawai', 'LIKE', '%'.$cari.'%')->get();
            return response()->json($data);
        }else{
            $data = DB::table('d_pegawai')->select('id', 'nama_pegawai')->get();
            return response()->json($data);
        }
    }
}
