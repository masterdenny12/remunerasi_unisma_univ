<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use Illuminate\Support\Carbon;

class NamaSekolahController extends Controller
{
    public function index(){

        // dd($queryy);
        return view('namasekolah.index');
    }

    public function show_data(){
        try {
            $result = [];
            $count = 1;

                $query = \DB::table('m_nama_sekolah')
                    ->select('*')
                    ->get();

            foreach ($query as $namasekolah) {
                $action_edit = '<center><a href="#" class="btn btn-success btn-sm m-btn  m-btn m-btn--icon" id="btn-edit-nama-sekolah"
                                data-id="' . $namasekolah->id . '"
                                data-nama_sekolah="' . $namasekolah->nama_sekolah . '"
                                data-keterangan="' . $namasekolah->keterangan . '"
                                data-toggle="modal"
                                data-target="#modal-edit-nama-sekolah">
                                <span>
                                    <i class="la la-archive"></i>
                                    <span>Update</span>
                                </span>
                                </a>';


                $action_del = '<a href="#" class="btn btn-danger m-btn btn-sm m-btn m-btn--icon" id="btn-delete-nama-sekolah"
                                data-id="' . $namasekolah->id . '">
                                <span>
                                    <i class="la la-warning"></i>
                                    <span>Delete</span>
                                </span>
                                </a></center>';

                $update = $namasekolah->updated_at ? \Carbon\Carbon::parse($namasekolah->updated_at)->format('d-m-Y H:i') : '';
                $data = [];
                $data[] = $count++;
                $data[] = strtoupper($namasekolah->nama_sekolah);
                $data[] = ($namasekolah->keterangan);
                $data[] = $update;
                $data[] = $action_edit.' '.$action_del;
                $result[] = $data;
            }
            return response()->json(['result' => $result]);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 406);
        }
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'nama_sekolah' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }
        $date = Carbon::now();

        DB::table('m_nama_sekolah')
                ->insert(['nama_sekolah' => $request->nama_sekolah,
                            'keterangan'  => $request->keterangan,
                            'created_at'    => $date
                ]);

        return response()->json(['success'=>'Data berhasil ditambahkan']);
    }

    public function update(Request $request)
    {
        $rules = [
            'id' => 'required',
            'nama_sekolah' => 'required',

        ];
        $messages = [
            'required' => 'The :attribute is required.',
            'min' => 'The :attribute is lest than 3 character.',
        ];
        //validation roles
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->all()], 406);
        }

        try {
            \DB::table('m_nama_sekolah')->where('id', $request->id)->update([
                'nama_sekolah' => $request->nama_sekolah,
                'keterangan' => $request->keterangan,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
            return response()->json(['status' => 'success', 'result' => 'Data berhasil diubah'], 200);
        } catch (\Exception $exception) {
            return response()->json(['status' => 'error', 'message' => $exception->getMessage()], 406);
        }
    }

    public function destroy(Request $request)
    {
        try {
            \DB::table('m_nama_sekolah')->where('id', '=', $request->id)->delete();

        } catch (Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 404);
        }
        return response()->json(['status' => 'success', 'result' => 'Data berhasil dihapus'], 200);
    }

}
