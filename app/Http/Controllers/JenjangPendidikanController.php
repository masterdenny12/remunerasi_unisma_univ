<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use Illuminate\Support\Carbon;

class JenjangPendidikanController extends Controller
{
    public function index(){

        // dd($queryy);
        return view('jenjangpendidikan.index');
    }

    public function show_data(){
        try {
            $result = [];
            $count = 1;

                $query = \DB::table('m_jenjang_pendidikan')
                    ->select('*')
                    ->get();

            foreach ($query as $jenjang_pendidikan) {
                $action_edit = '<center><a href="#" class="btn btn-success btn-sm m-btn  m-btn m-btn--icon" id="btn-edit-jenjang-pendidikan"
                                data-id="' . $jenjang_pendidikan->id . '"
                                data-nm_pendidikan="' . $jenjang_pendidikan->nm_pendidikan . '"
                                data-keterangan="' . $jenjang_pendidikan->keterangan . '"
                                data-toggle="modal"
                                data-target="#modal-edit-jenjang-pendidikan">
                                <span>
                                    <i class="la la-archive"></i>
                                    <span>Update</span>
                                </span>
                                </a>';


                $action_del = '<a href="#" class="btn btn-danger m-btn btn-sm m-btn m-btn--icon" id="btn-delete-jenjang-pendidikan"
                                data-id="' . $jenjang_pendidikan->id . '">
                                <span>
                                    <i class="la la-warning"></i>
                                    <span>Delete</span>
                                </span>
                                </a></center>';

                $update = $jenjang_pendidikan->updated_at ? \Carbon\Carbon::parse($jenjang_pendidikan->updated_at)->format('d-m-Y H:i') : '';
                $data = [];
                $data[] = $count++;
                $data[] = strtoupper($jenjang_pendidikan->nm_pendidikan);
                $data[] = ($jenjang_pendidikan->keterangan);
                $data[] = $update;
                $data[] = $action_edit.' '.$action_del;
                $result[] = $data;
            }
            return response()->json(['result' => $result]);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 406);
        }
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'nm_pendidikan' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }
        $date = Carbon::now();

        DB::table('m_jenjang_pendidikan')
                ->insert(['nm_pendidikan' => $request->nm_pendidikan,
                            'keterangan'  => $request->keterangan,
                            'created_at'    => $date
                ]);

        return response()->json(['success'=>'Data berhasil ditambahkan']);
    }

    public function update(Request $request)
    {
        $rules = [
            'id' => 'required',
            'nm_pendidikan' => 'required',

        ];
        $messages = [
            'required' => 'The :attribute is required.',
            'min' => 'The :attribute is lest than 3 character.',
        ];
        //validation roles
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->all()], 406);
        }

        try {
            \DB::table('m_jenjang_pendidikan')->where('id', $request->id)->update([
                'nm_pendidikan' => $request->nm_pendidikan,
                'keterangan' => $request->keterangan,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
            return response()->json(['status' => 'success', 'result' => 'Data berhasil diubah'], 200);
        } catch (\Exception $exception) {
            return response()->json(['status' => 'error', 'message' => $exception->getMessage()], 406);
        }
    }

    public function destroy(Request $request)
    {
        try {
            \DB::table('m_jenjang_pendidikan')->where('id', '=', $request->id)->delete();

        } catch (Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 404);
        }
        return response()->json(['status' => 'success', 'result' => 'Data berhasil dihapus'], 200);
    }

}
