<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use Validator;
use App\Model\m_pegawai;
use Illuminate\Support\Facades\Storage;
use File;

class PegawaiController extends Controller
{
    public function index()
    {
        return view('pegawai.index');
    }
    public function show_data(){
        try {
            $result = [];
            $count = 1;
            $query = DB::table('d_pegawai')
                    ->select('*')
                    ->get();
            foreach ($query as $pegawai) {
//                result for select2

                $action_edit = '<center><a href="#" class="btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only"
                                id="btn_detail_emp" data-emp_id="' . $pegawai->id . '">
                                <i class="fa flaticon-edit"></i>
                                </a>';


                $action_del = '<a href="#" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only" id="btn-delete-pegawai"
                                data-id="' . $pegawai->id . '">
                                <i class="fa flaticon-circle"></i>
                                </a></center>';

                $update = $pegawai->updated_at ? \Carbon\Carbon::parse($pegawai->updated_at)->format('d-m-y H:i') : '';
                $data = [];
                $data[] = $count++;
                $data[] = $pegawai->nip;
                $data[] = $pegawai->nama_pegawai;
                $data[] = $pegawai->status_pegawai == 'aktif' ? '<span class="m-badge m-badge--success m-badge--wide m-badge--rounded">'.$pegawai->status_pegawai.'</span>' :
                    '<span class="m-badge m-badge--danger m-badge--wide m-badge--rounded">'.$pegawai->status_pegawai.'</span>';
                $data[] = $update;
                $data[] = $action_edit.' '.$action_del;
                $result[] = $data;
            }
            return response()->json(['result' => $result]);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 406);
        }
    }

    public function AjaxDetailEmployee($emp_id)
    {
        $employee = \DB::table('d_pegawai as p')
            ->leftJoin('m_kota as k', 'p.kota_id', 'k.id')
            ->leftJoin('m_status_perkawinan as sp', 'p.status_perkawinan_id', 'sp.id')
            ->leftJoin('m_status_ikatan_kerja as ik', 'p.status_ikatan_kerja_id', 'ik.id')
            ->leftJoin('m_jenjang_pendidikan as jp', 'p.pendidikan_terakhir_id', 'jp.id')
            ->leftJoin('m_kategori_pegawai as kp', 'p.kategori_pegawai_id', 'kp.id')
            ->select('p.id', 'p.nip', 'p.nama_pegawai', 'p.tanggal_lahir', 'p.jenis_kelamin',
            'p.no_telp', 'p.foto', 'p.alamat', 'p.kota_id', 'k.nama_kota', 'p.tgl_mulai_masuk', 'p.status_pegawai',
            'p.status_perkawinan_id', 'sp.status_perkawinan', 'p.status_ikatan_kerja_id', 'ik.ikatan_kerja',
            'p.pendidikan_terakhir_id', 'jp.nm_pendidikan', 'p.kategori_pegawai_id', 'kp.jenis_pegawai'
            )
            ->where('p.id', $emp_id)
            ->first();
        // dd($employee);
        return response()->json(['status'=> 'success', 'result'=> $employee], 200);

    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'nip' => 'required',
            'nama_pegawai' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }
        $date = Carbon::now();

        if($request->foto == null){
            $request->foto == null;
        }else{
            $image = $request->file('foto');

            if ($image) {
                $pegawai_image = $request->nip. '.' . $image->getClientOriginalExtension();
                $filepath = public_path('foto');
                if (!is_dir($filepath)) {
                    \File::makeDirectory(public_path('foto'));
                }
                $upload = $image->move($filepath, $pegawai_image);
                if ($upload) {
                    $request->foto = $pegawai_image;
                }
            } else {
                $request->foto = null;
            }
        }

        m_pegawai::insert(['nip' => $request->nip,
                            'nama_pegawai'  => $request->nama_pegawai,
                            'tanggal_lahir' => $request->tanggal_lahir,
                            'tanggal_lahir'         => $request->tanggal_lahir,
                            'jenis_kelamin'         => $request->jenis_kelamin,
                            'kota_id'               => $request->kota_id,
                            'alamat'                => $request->alamat,
                            'no_telp'               => $request->no_telp,
                            'kategori_pegawai_id'   => $request->kategori_pegawai_id,
                            'foto'                  => $request->foto,
                            'status_perkawinan_id'  => $request->status_perkawinan_id,
                            'status_ikatan_kerja_id'   => $request->status_ikatan_kerja_id,
                            'status_pegawai'        => $request->status_pegawai,
                            'tgl_mulai_masuk'       => $request->tanggal_mulai_masuk,
                            'pendidikan_terakhir_id'   => $request->pendidikan_terakhir_id,
                            'created_at'    => $date
                ]);

        return response()->json(['success'=>'Data berhasil ditambahkan']);
    }

    public function update(Request $request)
    {
        // dd($request->all());
        $rules = [
            'nip' => 'required',
            'nama_pegawai' => 'required'
        ];
        $messages = [
            'required' => 'The :attribute is required.',
            'min' => 'The :attribute is lest than 3 character.',
        ];
        //validation roles
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->all()], 406);
        }
        $query_check = \DB::table('d_pegawai')->where('nip', $request->nip)->first();
        $image = $request->file('foto_edit');

        if ($image) {
            if(File::exists(trim(public_path('foto').'/'.$query_check->foto))){
                    // unlink(trim(public_path('foto').'/'.$query_check->foto));
                    Storage::delete(trim(public_path('foto').'/'.$query_check->foto));
            }
            $employee_image = $request->nip. '.' . $image->getClientOriginalExtension();
            $filepath = public_path('foto');
            if (!is_dir($filepath)) {
                \File::makeDirectory(public_path('foto'));
            }
            $upload = $image->move($filepath, $employee_image);
            if ($upload) {
                $upload_db = $employee_image;
            }
        } else {
            $upload_db = $query_check->foto;
        }

        //  dd($request->all());
        try {
           DB::table('d_pegawai')->where('nip', $request->nip)->update([
                'nip' => $request->nip,
                'nama_pegawai'          => $request->nama_pegawai,
                'tanggal_lahir'         => $request->tanggal_lahir,
                'no_telp'               => $request->no_telp,
                'jenis_kelamin'         => $request->jenis_kelamin,
                'kota_id'               => $request->kota_id,
                'alamat'                => $request->alamat,
                'kategori_pegawai_id'   => $request->kategori_pegawai_id,
                'foto'                  => $upload_db,
                'status_perkawinan_id'  => $request->status_perkawinan_id,
                'status_ikatan_kerja_id'   => $request->status_ikatan_kerja_id,
                'status_pegawai'        => $request->status_pegawai,
                'tgl_mulai_masuk'       => $request->tanggal_mulai_masuk,
                'pendidikan_terakhir_id'   => $request->pendidikan_terakhir_id,
                'created_at'        => \Carbon\Carbon::now(),
                'updated_at'        => \Carbon\Carbon::now()
            ]);
            // dd($request->all());
            return response()->json(['status' => 'success', 'result' => 'Data berhasil diubah'], 200);
        } catch (\Exception $exception) {
            return response()->json(['status' => 'error', 'message' => $exception->getMessage()], 406);
        }
    }

    public function destroy(Request $request)
    {
        try {
            m_pegawai::where('id', '=', $request->id)->delete();

        } catch (Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 404);
        }
        return response()->json(['status' => 'success', 'result' => 'Data berhasil dihapus'], 200);
    }
}
