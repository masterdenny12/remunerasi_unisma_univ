<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use Illuminate\Support\Carbon;

class KategoriPenilaianController extends Controller
{
    public function index(){

        // dd($queryy);
        return view('kategoripenilaian.index');
    }


    public function show_sub_kategori_dosen($id_kategori){
        $query = \DB::table('m_kategori_penilaian_dosen')
        ->where('id', $id_kategori)
        ->select('*')
        ->first();
        // dd($query);
        return view('kategoripenilaian.sub_kategori', compact('query'));
    }
    public function show_komponen_kompetensi($sub_kategori_dosen_id){
        try {
            $result = [];
            $count = 1;

                $query = \DB::table('m_komponen_kompetensi_dosen')
                    ->select('*')
                    ->where('sub_kategori_dosen_id',$sub_kategori_dosen_id)
                    ->get();

            foreach ($query as $kategori_penilaian) {
                $action_edit = '<center><a href="#" class="btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only"
                                id="btn_detail_emp" data-emp_id="' . $kategori_penilaian->id . '">
                                <i class="fa flaticon-edit"></i>
                                </a>';


                $action_del = '<a href="#" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only" id="btn-delete-pegawai"
                                data-id="' . $kategori_penilaian->id . '">
                                <i class="fa flaticon-circle"></i>
                                </a></center>';

                $update = $kategori_penilaian->updated_at ? \Carbon\Carbon::parse($kategori_penilaian->updated_at)->format('d-m-Y H:i') : '';
                $data = [];
                $data[] = $count++;
                $data[] = strtoupper($kategori_penilaian->komponen_kompetensi);
                $data[] = $kategori_penilaian->deskripsi_skor1;
                $data[] = $kategori_penilaian->deskripsi_skor2;
                $data[] = $kategori_penilaian->deskripsi_skor3;
                $data[] = $kategori_penilaian->deskripsi_skor4;
                $data[] = $kategori_penilaian->deskripsi_skor5;
                $data[] = $action_edit.' '.$action_del;
                $result[] = $data;
            }
            return response()->json(['result' => $result]);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 406);
        }
    }
    public function show_data(){
        try {
            $result = [];
            $count = 1;

                $query = \DB::table('m_kategori_penilaian_dosen')
                    ->select('*')
                    ->get();

            foreach ($query as $kategori_penilaian) {
                $action_edit = '<center>
                                <a href="show_sub_kategori_dosen/'.$kategori_penilaian->id.'" class="btn btn-primary btn-sm m-btn  m-btn m-btn--icon" id="btn-edit-kategori-penilaian"
                                data-id="' . $kategori_penilaian->id . '"
                                data-kategori="' . $kategori_penilaian->kategori . '"
                                data-bobot="' . $kategori_penilaian->bobot . '"
                                data-keterangan="' . $kategori_penilaian->keterangan . '"
                                >
                                <span>
                                    <i class="la la-list"></i>
                                    <span>Sub kategori</span>
                                </span>
                                </a>
                                <a href="#" class="btn btn-success btn-sm m-btn  m-btn m-btn--icon" id="btn-edit-kategori-penilaian"
                                data-id="' . $kategori_penilaian->id . '"
                                data-kategori="' . $kategori_penilaian->kategori . '"
                                data-bobot="' . $kategori_penilaian->bobot . '"
                                data-keterangan="' . $kategori_penilaian->keterangan . '"
                                data-toggle="modal"
                                data-target="#modal-edit-kategori-penilaian">
                                <span>
                                    <i class="la la-edit"></i>
                                    <span>Update</span>
                                </span>
                                </a>';



                $action_del = '<a href="#" class="btn btn-danger m-btn btn-sm m-btn m-btn--icon" id="btn-delete-kategori-penilaian"
                                data-id="' . $kategori_penilaian->id . '">
                                <span>
                                    <i class="la la-warning"></i>
                                    <span>Delete</span>
                                </span>
                                </a></center>';

                $update = $kategori_penilaian->updated_at ? \Carbon\Carbon::parse($kategori_penilaian->updated_at)->format('d-m-Y H:i') : '';
                $data = [];
                $data[] = $count++;
                $data[] = strtoupper($kategori_penilaian->kategori);
                $data[] = ($kategori_penilaian->keterangan);
                $data[] = ($kategori_penilaian->bobot * 100 ).'%';
                $data[] = $update;
                $data[] = $action_edit.' '.$action_del;
                $result[] = $data;
            }
            return response()->json(['result' => $result]);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 406);
        }
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'kategori' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }
        $date = Carbon::now();
        $persen_bobot = $request->bobot / 100;
        DB::table('m_kategori_penilaian_dosen')
                ->insert(['kategori' => $request->kategori,
                            'bobot'  => $persen_bobot,
                            'keterangan'  => $request->keterangan,
                            'created_at'    => $date
                ]);

        return response()->json(['success'=>'Data berhasil ditambahkan']);
    }

    public function update(Request $request)
    {
        $rules = [
            'id' => 'required',
            'kategori' => 'required',

        ];
        $messages = [
            'required' => 'The :attribute is required.',
            'min' => 'The :attribute is lest than 3 character.',
        ];
        //validation roles
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->all()], 406);
        }
        $persen_bobot =  $request->bobot / 100;
        try {
            \DB::table('m_kategori_penilaian_dosen')->where('id', $request->id)->update([
                'kategori' => $request->kategori,
                'keterangan' => $request->keterangan,
                'bobot' => $persen_bobot,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
            return response()->json(['status' => 'success', 'result' => 'Data berhasil diubah'], 200);
        } catch (\Exception $exception) {
            return response()->json(['status' => 'error', 'message' => $exception->getMessage()], 406);
        }
    }

    public function destroy(Request $request)
    {
        try {
            \DB::table('m_kategori_penilaian_dosen')->where('id', '=', $request->id)->delete();

        } catch (Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 404);
        }
        return response()->json(['status' => 'success', 'result' => 'Data berhasil dihapus'], 200);
    }

}
