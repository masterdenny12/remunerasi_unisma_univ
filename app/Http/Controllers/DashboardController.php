<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        $tot_dosen = DB::table('d_pegawai')
        ->where('kategori_pegawai_id', 1)
        ->count();
        $tot_karyawan = DB::table('d_pegawai')
        ->where('kategori_pegawai_id', 2)
        ->count();
        $tot_pejabat = DB::table('d_pegawai')
        ->where('kategori_pegawai_id', 3)
        ->count();
        $all_pegawai = DB::table('d_pegawai')
        ->count();

        $summary_pegawai = \DB::table('d_pegawai as k')
            ->Leftjoin('m_kategori_pegawai as kp', 'k.kategori_pegawai_id', 'kp.id')
            ->select(\DB::raw('count(k.id) as semua_karyawan, k.id, kp.jenis_pegawai'))
            ->groupBy('k.id', 'kp.jenis_pegawai')
            ->orderBy('semua_karyawan', 'DESC')
            ->get();
        $dataPoint = [];
        foreach ($summary_pegawai as $pegawai){
            $dataPoint []= [
                "y" => $pegawai->semua_karyawan,
                "label"  => $pegawai->jenis_pegawai,
            ];
        }
        $summary_emp = [
            'dataPoint' => $dataPoint,
        ];

        return view('dashboard.index', compact('tot_dosen', 'tot_karyawan', 'tot_pejabat', 'all_pegawai', 'summary_emp'));
    }

}
