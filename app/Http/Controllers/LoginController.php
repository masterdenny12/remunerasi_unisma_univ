<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use App\User;

class LoginController extends Controller
{
    public function index(){
        return view('login.index');
    }

    public function postRegister(){
        $query = User::insert(['name'       => 'superadmin',
                                'username'  => 'admin',
                                'email'     => 'admin@gmail.com',
                                'password'  => bcrypt('admin'),
                                'role_id'   => 1
        ]);
    }

    public function postLogin(Request $request)
    {
        request()->validate([
        'username' => 'required',
        'password' => 'required',
        ]);

        $credentials = $request->only('username', 'password');

        if (Auth::attempt($credentials)) {
                return redirect()->intended('dashboard');
        }else
                return redirect()->back()->with('error_login', 'username/password salah');

    }

    public function logout() {
        Session::flush();
        Auth::logout();
        return Redirect('/');
    }
}
