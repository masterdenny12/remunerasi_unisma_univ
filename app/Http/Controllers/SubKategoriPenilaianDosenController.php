<?php

namespace App\Http\Controllers;
use Illuminate\Support\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SubKategoriPenilaianDosenController extends Controller
{
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'sub_kategori' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }
        $date = Carbon::now();

        DB::table('m_sub_kategori_penilaian_dosen')
                ->insert(['kategori_penilaian_id' => $request->kategori_penilaian_id,
                            'sub_kategori'  => $request->sub_kategori,
                            'created_at'    => $date
                ]);

        return response()->json(['success'=>'Data berhasil ditambahkan']);
    }

    public function show_kategori($id_kategori){
        try {
            $result = [];
            $count = 1;

                $query = \DB::table('m_sub_kategori_penilaian_dosen')
                    ->where('kategori_penilaian_id', $id_kategori)
                    ->select('*')
                    ->get();

            foreach ($query as $kategori_penilaian) {
                $action_edit = '<center>
                                <a href="" class="btn btn-primary btn-sm m-btn  m-btn m-btn--icon" id="btn-edit-kategori-penilaian"
                                data-id="' . $kategori_penilaian->id . '"
                                data-sub_kategori="' . $kategori_penilaian->sub_kategori . '"
                                data-toggle="modal"
                                data-target="#modal-komponen-kompetensi"
                                >
                                <span>
                                    <i class="la la-list"></i>
                                    <span>Kompetensi</span>
                                </span>
                                </a>
                                <a href="#" class="btn btn-success btn-sm m-btn  m-btn m-btn--icon" id="btn-edit-kategori-penilaian"
                                data-id="' . $kategori_penilaian->id . '"
                                data-sub_kategori="' . $kategori_penilaian->sub_kategori . '"
                                data-toggle="modal"
                                data-target="#modal-edit-sub-kategori-penilaian">
                                <span>
                                    <i class="la la-edit"></i>
                                    <span>Update</span>
                                </span>
                                </a>';



                                $action_del = '<a href="#" class="btn btn-danger m-btn btn-sm m-btn m-btn--icon" id="btn-delete-sub-kategori-penilaian"
                                data-id="' . $kategori_penilaian->id . '">
                                <span>
                                    <i class="la la-warning"></i>
                                    <span>Hapus</span>
                                </span>
                                </a></center>';

                $update = $kategori_penilaian->updated_at ? \Carbon\Carbon::parse($kategori_penilaian->updated_at)->format('d-m-Y H:i') : '';
                $data = [];
                $data[] = $count++;
                $data[] = strtoupper($kategori_penilaian->sub_kategori);
                $data[] = $update;
                $data[] = $action_edit.' '.$action_del;
                $result[] = $data;
            }
            return response()->json(['result' => $result]);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 406);
        }
    }

    public function update(Request $request)
    {
        $rules = [
            'sub_kategori' => 'required',

        ];
        $messages = [
            'required' => 'The :attribute is required.',
            'min' => 'The :attribute is lest than 3 character.',
        ];
        //validation roles
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->all()], 406);
        }
        $persen_bobot =  $request->bobot / 100;
        try {
            \DB::table('m_sub_kategori_penilaian_dosen')->where('id', $request->id)->update([
                'sub_kategori' => $request->sub_kategori,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
            return response()->json(['status' => 'success', 'result' => 'Data berhasil diubah'], 200);
        } catch (\Exception $exception) {
            return response()->json(['status' => 'error', 'message' => $exception->getMessage()], 406);
        }
    }

    public function destroy(Request $request)
    {
        try {
            \DB::table('m_sub_kategori_penilaian_dosen')->where('id', '=', $request->id)->delete();

        } catch (Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 404);
        }
        return response()->json(['status' => 'success', 'result' => 'Data berhasil dihapus'], 200);
    }
}
