<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class m_pegawai extends Model
{
    protected $table = 'd_pegawai';
    protected $fillable = ['nip', 'nama_pegawai', 'created_at'];
}
