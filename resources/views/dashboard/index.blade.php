@extends('master')
@section('konten')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
<div class="m-subheader" style="padding: 0px">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">Dashboard</h3>
								<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="#" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-home"></i>
										</a>
									</li>

								</ul>
							</div>
							<div>

							</div>
						</div>
                    </div>
                    <br>
<div class="m-portlet ">
							<div class="m-portlet__body  m-portlet__body--no-padding">
								<div class="row m-row--no-padding m-row--col-separator-xl">
									<div class="col-md-12 col-lg-6 col-xl-3">

										<!--begin::Total Profit-->
										<div class="m-widget24">
											<div class="m-widget24__item">
												<h4 class="m-widget24__title">
													Dosen
												</h4><br>
												<span class="m-widget24__desc">
													Total Dosen
												</span>
												<span class="m-widget24__stats m--font-brand">
													{{$tot_dosen}}
												</span>
												<div class="m--space-10"></div>
												<div class="progress m-progress--sm">
													<div class="progress-bar m--bg-brand" role="progressbar" style="width: 78%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<span class="m-widget24__change">
													Change
												</span>
												<span class="m-widget24__number">
													78%
												</span>
											</div>
										</div>

										<!--end::Total Profit-->
									</div>
									<div class="col-md-12 col-lg-6 col-xl-3">

										<!--begin::New Feedbacks-->
										<div class="m-widget24">
											<div class="m-widget24__item">
												<h4 class="m-widget24__title">
													Karyawan
												</h4><br>
												<span class="m-widget24__desc">
													Total karyawan
												</span>
												<span class="m-widget24__stats m--font-info">
													{{$tot_karyawan}}
												</span>
												<div class="m--space-10"></div>
												<div class="progress m-progress--sm">
													<div class="progress-bar m--bg-info" role="progressbar" style="width: 84%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<span class="m-widget24__change">
													Change
												</span>
												<span class="m-widget24__number">
													84%
												</span>
											</div>
										</div>

										<!--end::New Feedbacks-->
									</div>
									<div class="col-md-12 col-lg-6 col-xl-3">

										<!--begin::New Orders-->
										<div class="m-widget24">
											<div class="m-widget24__item">
												<h4 class="m-widget24__title">
                                                    Pejabat
												</h4><br>
												<span class="m-widget24__desc">
													Total Pejabat
												</span>
												<span class="m-widget24__stats m--font-danger">
													{{$tot_pejabat}}
												</span>
												<div class="m--space-10"></div>
												<div class="progress m-progress--sm">
													<div class="progress-bar m--bg-danger" role="progressbar" style="width: 69%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<span class="m-widget24__change">
													Change
												</span>
												<span class="m-widget24__number">
													69%
												</span>
											</div>
										</div>

										<!--end::New Orders-->
									</div>
									<div class="col-md-12 col-lg-6 col-xl-3">

										<!--begin::New Users-->
										<div class="m-widget24">
											<div class="m-widget24__item">
												<h4 class="m-widget24__title">
													Pegawai
												</h4><br>
												<span class="m-widget24__desc">
													Semua Pegawai
												</span>
												<span class="m-widget24__stats m--font-success">
													{{$all_pegawai}}
												</span>
												<div class="m--space-10"></div>
												<div class="progress m-progress--sm">
													<div class="progress-bar m--bg-success" role="progressbar" style="width: 90%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<span class="m-widget24__change">
													Change
												</span>
												<span class="m-widget24__number">
													90%
												</span>
											</div>
										</div>

										<!--end::New Users-->
									</div>
								</div>
							</div>
</div>

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <i class="m-menu__link-icon flaticon-graph"></i>&nbsp Chart Pegawai
                </h3>
            </div>
        </div>

    </div>
        <!--begin: Datatable -->

            <!-- column -->
                            <div class="col-md-12">
                                <!--begin:: Widgets/Daily Sales-->
                                <div class="m-widget14">
                                    <div class="m-widget14__header m--margin-bottom-30">
                                        <h3 class="m-widget14__title">
                                        </h3>
                                    </div>
                                        <div id="chartContainer" style="height: 300px; width: 100%;"></div>
                                </div>
                            </div>



            <!-- column -->


</div>

</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

<script>
    function config(){
        var config = {
            theme: "light4", // "light1", "light2", "dark1", "dark2"
            exportEnabled: true,
            animationEnabled: true,
            title: {
                {{--  text: "Total Employee"  --}}
            },
            data: [{
                type: "pie",
                startAngle: 50,
                toolTipContent: "<b>{label}</b>: {y}",
                showInLegend: "true",
                legendText: "{label}",
                indexLabelFontSize: 20,
                indexLabel: "{label} - {y}",
                dataPoints: {!! json_encode($summary_emp['dataPoint'], JSON_NUMERIC_CHECK) !!}
            }]
        }
        return config;
    }
    $(document).ready(function () {
        var chart = new CanvasJS.Chart("chartContainer", config());
        chart.render();
    })
</script>

@endsection
