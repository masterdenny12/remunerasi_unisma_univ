@extends('master')
@section('konten')
<div class="m-portlet m-portlet--mobile">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											<i class="m-menu__link-icon flaticon-list"></i>&nbsp Master Jenjang pendidikan
										</h3>
									</div>
								</div>
								<div class="m-portlet__head-tools">
									<ul class="m-portlet__nav">
										<li class="m-portlet__nav-item">
											<a href="#" data-toggle="modal" data-target="#CreateJenjangPendidikanModal" class="btn btn-success m-btn m-btn--icon m-btn--air">
												<span>
													<i class="la la-plus"></i>
													<span>Tambah Data</span>
												</span>
											</a>
										</li>
										<li class="m-portlet__nav-item"></li>
										<li class="m-portlet__nav-item">
											<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
												<a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
													<i class="la la-ellipsis-h m--font-brand"></i>
												</a>

											</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="m-portlet__body">

								<!--begin: Datatable -->
								<table class="table table-striped- table-bordered table-hover tabel_show" >
									<thead>
										<tr>
                                            <th>No.</th>
                                            <th>Jenjang pendidikan</th>
                                            <th>Keterangan</th>
											<th>Update at</th>
											<th>Aksi</th>

										</tr>
									</thead>

								</table>
							</div>
						</div>


 <!-- Create Modal -->
 <div class="modal" id="CreateJenjangPendidikanModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Tambah Jenjang pendidikan</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                    <strong>Success!</strong>Product was added successfully.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="form-group">
                    <label for="Name">Jenjang pendidikan:</label>
                    <input type="text" class="form-control" placeholder="Jenjang pendidikan" name="nm_pendidikan" id="tambah_nm_pendidikan">
                </div>
                <div class="form-group">
                    <label for="Name">Keterangan:</label>
                    <input type="text" class="form-control" placeholder="keterangan" name="keterangan" id="tambah_keterangan">
                </div>

            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="SubmitCreateForm">Create</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

       {{--modal edit ikatan kerja--}}
       <div class="modal" id="modal-edit-jenjang-pendidikan" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="form_edit_jenjang_pendidikan" method="post">
                    <div class="modal-body">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Ikatan kerja:</label>
                            <input type="hidden" name="id" class="form-control" id="txtedit_id">
                            <input type="text" placeholder="Jenjang pendidikan" name="nm_pendidikan" class="form-control" id="txtedit_nm_pendidikan" required>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Keterangan:</label>
                            <input type="text" placeholder="Keterangan" name="keterangan" class="form-control" id="txtedit_keterangan"
                                   >
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success btn-flat">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
    <script src="js_crud/index.js"></script>
<script>

    $(document).ready(function(){

        var table_ikatan_kerja = $('.tabel_show').DataTable({
            responsive:true,
            paging: true,
            info: true,
            searching: true,
            "aaSorting": [],
            "ordering": true,
            ajax: {
                url: '{{url("datatable_jenjang_pendidikan")}}',
                dataSrc: 'result',
            },

        });

        $('#SubmitCreateForm').click(function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{ url('simpan_master_jenjang_pendidikan') }}",
                method: 'post',
                data: {
                    nm_pendidikan: $('#tambah_nm_pendidikan').val(),
                    keterangan: $('#tambah_keterangan').val(),

                },
                success: function(result) {
                    if(result.errors) {
                        $('.alert-danger').html('');
                        $.each(result.errors, function(key, value) {
                            $('.alert-danger').show();
                            $('.alert-danger').append('<strong><li>'+value+'</li></strong>');
                        });
                    } else {
                        $('.alert-danger').hide();
                        $('.alert-success').show();
                        swal.fire({
                            title: "Information!",
                            html: '<strong>' + result.success + '</strong>',
                            type:'success',
                            button: false,
                            timer: 2000,
                        })
                        $('.tabel_show').DataTable().ajax.reload();
                            $('.alert-success').hide();
                            $('#CreateJenjangPendidikanModal').modal('hide');


                    }
                }
            });
        });

        $('#form_edit_jenjang_pendidikan').submit(function (event) {
            $('#modal-edit-jenjang-pendidikan').modal('hide');
            event.preventDefault();
            var url = '{{ url('update_master_jenjang_pendidikan') }}';
            ajaxProcess(url, $(this).serialize())
        });

        function ajaxProcess(url, data) {
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                beforeSend: function () {
                    $('.cssload-container').show();
                },
                success: function (response) {
                    if (response.status == 'success') {
                        $('.cssload-container').hide();

                        $('.tabel_show').DataTable().ajax.reload();


                        Swal.fire({
                            title: 'Information',
                            html: '<strong>' + response.result + '</strong>',
                            type: 'success',
                            showConfirmButton: false,
                            timer: 3000
                        });
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error);
                        var result = error.responseJSON;
                        if (result != null) {
                            var message = result.message;
                            if (Array.isArray(message)) {
                                $.map(message, function (value, index) {
                                    message = value + '</br>';
                                });
                            }
                        } else {
                            message = 'look like something when wrong';
                        }
                    } else {
                        message = 'look like something when wrong';
                    }


                    $('#form_edit_jenjang_pendidikan')[0].reset();
                    $('.cssload-container').hide();
                    Swal.fire({
                        title: 'Warning',
                        html: '<strong>' + message + '</strong>',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 3000
                    })

                }

            })
        };


        //show detail data
        modal_edit_ikatan_kerja();

        function modal_edit_ikatan_kerja() {
            $('#modal-edit-jenjang-pendidikan').on('show.bs.modal', function (event) {
                // do something...
                var button = $(event.relatedTarget) // Button that triggered the modal
                var id = button.data('id');
                var nm_pendidikan = button.data('nm_pendidikan');
                var keterangan = button.data('keterangan');
                $('#txtedit_id').val(id);
                $('#txtedit_nm_pendidikan').val(nm_pendidikan);
                $('#txtedit_keterangan').val(keterangan);
            });
        }

       $('.tabel_show tbody').on('click', '#btn-delete-jenjang-pendidikan', function () {
        var url = '{{ url('destroy_master_jenjang_pendidikan') }}';
        var id = $(this).attr('data-id');
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var jenjang_pendidikan = {_token: CSRF_TOKEN, id: id};
        delete_master(url, jenjang_pendidikan);
        });

    });

</script>
@endsection

