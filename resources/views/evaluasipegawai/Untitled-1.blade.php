<table border="1" class="rwd-table" style="width: 100%">
    <tr>
        <TH rowspan="2">NO</TH>
        <TH  rowspan="2" style="text-align: center"> Komponen Kompetensi</TH>
        <TH colspan="5" style="text-align: center"> Skor</TH>
        <TH rowspan="2" style="text-align: center"> Skor</TH>
        <TH rowspan="2" style="text-align: center"> Skor/Skor maks</TH>
        <TH rowspan="2" style="text-align: center"> Skor x Bobot sub</TH>

    </tr>
    <tr >
      <TH style="text-align: center">1</TH><TH style="text-align: center">2</TH><TH style="text-align: center">3</TH>
      <TH style="text-align: center">4</TH><TH style="text-align: center">5</TH>

    </tr>
    @php
        $no = 1;
    @endphp

    @foreach ($sub_kategori_penilaian_dosen as $key_sub_kategori_penilaian_dosen => $item)
    <tr>
        <td>{{$no++}}</td>
        <td colspan="10">{{$item['sub_kategori']}}</td>
    </tr>
    @foreach ($item['komponen_kompetensi'] as $key_komponen_kompetensi => $item_komponen_kompetensi)

    <tr>
        <tr><td></td>
            <td>- Deskripsi penilaian<br>
               - Bukti pendukung <b>( {{$item_komponen_kompetensi['bukti_pendukung']}} )</b>
            </td>
            <td>{{$item_komponen_kompetensi['skor_1']}}</td>
            <td style="width: 10%">{{$item_komponen_kompetensi['skor_2']}}</td>
            <td>{{$item_komponen_kompetensi['skor_3']}}</td>
            <td>{{$item_komponen_kompetensi['skor_4']}}</td>
            <td>{{$item_komponen_kompetensi['skor_5']}}</td>
            <td rowspan="2" style="text-align: center"><div id="skor_nilai_{{$item_komponen_kompetensi['id_komponen_kompetensi']}}">0</div></td>
            <td rowspan="2" style="text-align: center"><div id="skor_nilai_dibagi_maksimal_{{$item_komponen_kompetensi['id_komponen_kompetensi']}}">0</div></td>
            <td rowspan="2" style="text-align: center">
                <input type="hidden" id="value_bobot_{{$item_komponen_kompetensi['id_komponen_kompetensi']}}" value="{{$item_komponen_kompetensi['bobot']}}">
                <div id="perhitungan_bobot_{{$item_komponen_kompetensi['id_komponen_kompetensi']}}">0,000</div>
            </td>

        </tr>
        <td>A </td>
        <td >{{$item_komponen_kompetensi['judul_komponen_kompetensi']}}</td>
        <td style="text-align: center"><input data-id="{{$item_komponen_kompetensi['id_komponen_kompetensi']}}" class="nilai" type="radio"  name="{{$key_sub_kategori_penilaian_dosen.$key_komponen_kompetensi}}" value="1">
            <input type="hidden"  value="1">
        </td>
        <td style="text-align: center"><input data-id="{{$item_komponen_kompetensi['id_komponen_kompetensi']}}" class="nilai"  type="radio" name="{{$key_sub_kategori_penilaian_dosen.$key_komponen_kompetensi}}" value="2" >
            <input type="hidden" value="2">
        </td>
        <td style="text-align: center"><input data-id="{{$item_komponen_kompetensi['id_komponen_kompetensi']}}" class="nilai"  type="radio" name="{{$key_sub_kategori_penilaian_dosen.$key_komponen_kompetensi}}" value="3">
            <input type="hidden" value="3">
        </td>
        <td style="text-align: center"><input data-id="{{$item_komponen_kompetensi['id_komponen_kompetensi']}}" class="nilai"  type="radio" name="{{$key_sub_kategori_penilaian_dosen.$key_komponen_kompetensi}}" value="4">
            <input type="hidden" value="4">
        </td>
        <td style="text-align: center"><input data-id="{{$item_komponen_kompetensi['id_komponen_kompetensi']}}" class="nilai"  type="radio" name="{{$key_sub_kategori_penilaian_dosen.$key_komponen_kompetensi}}" value="5">
            <input type="hidden" value="5">
        </td>
    </tr>

    @endforeach
    @endforeach
</table>
