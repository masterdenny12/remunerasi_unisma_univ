@extends('master')
<head>
    <style>
        .rwd-table {
            margin: auto;
            min-width: 300px;
            max-width: 100%;
            border-collapse: collapse;
          }

          .rwd-table tr:first-child {
            border-top: none;
            background: #34bfa3;
            color: #fff;
          }

          .rwd-table tr {
            border-top: 1px solid #ddd;
            border-bottom: 1px solid #ddd;
            background-color: #f5f9fc;
          }

          .rwd-table tr:nth-child(odd):not(:first-child) {
            background-color: #ebf3f9;
          }

          .rwd-table th {
            display: none;
          }

          .rwd-table td {
            display: block;
          }

          .rwd-table td:first-child {
            margin-top: .3em;
          }

          .rwd-table td:last-child {
            margin-bottom: .3em;
          }

          .rwd-table td:before {
            content: attr(data-th) ": ";
            font-weight: bold;
            width: 120px;
            display: inline-block;
            color: #000;
          }

          .rwd-table th,
          .rwd-table td {
            text-align: left;
          }

          .rwd-table {
            color: #333;
            border-radius: .4em;
            overflow: hidden;
          }

          .rwd-table tr {
            border-color: #bfbfbf;
          }

          .rwd-table th,
          .rwd-table td {
            padding: .3em 0.5em;
          }
          @media screen and (max-width: 601px) {
            .rwd-table tr:nth-child(2) {
              border-top: none;
            }
          }
          @media screen and (min-width: 600px) {
            .rwd-table tr:hover:not(:first-child) {
              background-color: #d8e7f3;
            }
            .rwd-table td:before {
              display: none;
            }
            .rwd-table th,
            .rwd-table td {
              display: table-cell;
              padding: .10em .3em;
            }
            .rwd-table th:first-child,
            .rwd-table td:first-child {
              padding-left: 0;
            }
            .rwd-table th:last-child,
            .rwd-table td:last-child {
              padding-right: 0;
            }
            .rwd-table th,
            .rwd-table td {
              padding: 0.5em !important;
            }
          }
          .isDisabled {
            color: currentColor;
            cursor: not-allowed;
            opacity: 0.5;
            text-decoration: none;
          }

    </style>
</head>
@section('konten')
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <i class="m-menu__link-icon flaticon-list"></i>&nbsp Evaluasi Penilaian Pegawai
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">

                <li class="m-portlet__nav-item"></li>

            </ul>
        </div>
    </div>
    <div class="m-portlet__body">

        <!--begin: Datatable -->
        <div class="form-group">
            <div class="row">

                <div class="col-md-7">
                    <input type="text" class="form-control" placeholder="Masukkan Nama pegawai / NIP" name="nip" id="nip">
                </div>
                <div class="col-md-3">
                    <a id="nilai_evaluasi" class="btn btn-success m-btn m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-search"></i>
                            <span>Cari pegawai</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        {{--  <div class="form-group">
            <div class="row">
                <div class="col-md-2">
                    <label for="Name">Nama Pegawai:</label>
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control" disabled placeholder="Pegawai" name="nama_pegawai" id="input_nama_pegawai">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-2">
                    <label for="Name">Jabatan:</label>
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control" disabled placeholder="Jabatan" name="jabatan" id="input_jabatan">
                </div>
            </div>
        </div>  --}}

        <hr>
        <br>

    </div>

</div>



    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
    <script src="js_crud/index.js"></script>
<script>

    $(document).ready(function(){

       $('#nilai_evaluasi').click(function(){
        var nip =  $('#nip').val();
        $(this).attr('href', '{{url("hasil_evaluasi_penilaian_dosen/")}}' + '/' + nip);
       });


    });

</script>
@endsection

