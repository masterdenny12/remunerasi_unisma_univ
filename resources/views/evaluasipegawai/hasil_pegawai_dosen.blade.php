@extends('master_sub')
<head>

    <style>
        .rwd-table {
            margin: auto;
            min-width: 300px;
            max-width: 100%;
            border-collapse: collapse;
          }

          .rwd-table tr:first-child {
            border-top: none;
            background: #34bfa3;
            color: #fff;
          }

          .rwd-table tr {
            border-top: 1px solid #ddd;
            border-bottom: 1px solid #ddd;
            background-color: #f5f9fc;
          }

          .rwd-table tr:nth-child(odd):not(:first-child) {
            background-color: #ebf3f9;
          }

          .rwd-table th {
            display: none;
          }

          .rwd-table td {
            display: block;
          }

          .rwd-table td:first-child {
            margin-top: .3em;
          }

          .rwd-table td:last-child {
            margin-bottom: .3em;
          }

          .rwd-table td:before {
            content: attr(data-th) ": ";
            font-weight: bold;
            width: 120px;
            display: inline-block;
            color: #000;
          }

          .rwd-table th,
          .rwd-table td {
            text-align: left;
          }

          .rwd-table {
            color: #333;
            border-radius: .4em;
            overflow: hidden;
          }

          .rwd-table tr {
            border-color: #bfbfbf;
          }

          .rwd-table th,
          .rwd-table td {
            padding: .3em 0.5em;
          }
          @media screen and (max-width: 601px) {
            .rwd-table tr:nth-child(2) {
              border-top: none;
            }
          }
          @media screen and (min-width: 600px) {
            .rwd-table tr:hover:not(:first-child) {
              background-color: #d8e7f3;
            }
            .rwd-table td:before {
              display: none;
            }
            .rwd-table th,
            .rwd-table td {
              display: table-cell;
              padding: .10em .3em;
            }
            .rwd-table th:first-child,
            .rwd-table td:first-child {
              padding-left: 0;
            }
            .rwd-table th:last-child,
            .rwd-table td:last-child {
              padding-right: 0;
            }
            .rwd-table th,
            .rwd-table td {
              padding: 0.5em !important;
            }
          }
          .isDisabled {
            color: currentColor;
            cursor: not-allowed;
            opacity: 0.5;
            text-decoration: none;
          }

    </style>
</head>
@section('konten')
<div class="m-portlet m-portlet--mobile">

    <div class="m-portlet__body">
        <center><h4>EVALUASI PEGAWAI <br>UNIVERSITAS ISLAM MALANG (UNISMA)</h4></center><br>
        <table style="font-size: 12px;margin:15px">
            <tr>
                <td>
                    Nama pegawai
                </td>
                <td>:</td>
                <td><div id="input_nama_pegawai">&nbsp<b>{{($check_nip->nama_pegawai)}}</b></div></td>
            </tr>
            <tr>
                <td>
                   NIP
                </td>
                <td>:</td>
                <td><div id="input_nama_pegawai">&nbsp<b>{{($check_nip->nip)}}</b></div></td>
            </tr>
            <tr>
                <td>
                    Jabatan
                </td>
                <td>:</td>
                <td><div id="input_jabatan"></div></td>
            </tr>
            <tr>
                <td>
                    Program Studi
                </td>
                <td>:</td>
                <td><div id="input_program_studi"></div></td>
            </tr>
        </table>

        <div class="show_list_penilaian" style="margin:15px">
            <table class="rwd-table" border="" style="border-top: 10;width: 100%; font-size:12px; border-color:green">
                <tr>
                    <TH rowspan="2">NO</TH>
                    <TH  rowspan="2" style="text-align: center"> Komponen Kompetensi</TH>
                    <TH style="text-align: center"> Bobot</TH>
                    <TH style="text-align: center"> Skor Target</TH>
                    <TH style="text-align: center"> Skor Capaian</TH>
                    <TH style="text-align: center"> Persentase Capaian</TH>
                    <TH style="text-align: center"> Predikat</TH>
                    <TH style="text-align: center"> Nilai Capaian</TH>
                    <TH rowspan="2" style="text-align: center"> Aksi</TH>
                </tr>
                <tr>
                    <TH style="text-align: center">1</TH>
                    <TH style="text-align: center">2</TH>
                    <TH style="text-align: center">3</TH>
                    <TH style="text-align: center">4<br>(3 / 2) x 100 %</TH>
                    <TH style="text-align: center">5</TH>
                    <TH style="text-align: center">6 <br> (1 x 4)</TH>
                </tr>
                @php
                    $noo = 1;
                @endphp
                @foreach ($arr as $item)
                    <tr>
                        <td>
                            {{$noo++}}
                        </td>
                        <td>
                            <b>{{$item['kategori']}}</b>
                        </td>
                        <td>
                            <center>
                               {{$item['bobot'] * 100}} %
                            </center>
                        </td>
                        {{--  skor target  --}}
                        <td>
                            <center>
                               {{$item['skor_target_check']}}
                            </center>
                        </td>
                        {{--  skor capaian  --}}
                        <td>
                            <center>
                                {{$item['skor_total_capaian']}}
                            </center>
                        </td>
                        <td>
                            <center>
                                {{$item['presentase_capaian']}}
                            </center>
                        </td>
                        <td>
                            <center>
                                {{$item['predikat_total']}}
                            </center>
                        </td>
                        <td>
                            <center>
                                {{$item['nilai_capaian']}}
                            </center>
                        </td>
                        <td></td>
                    </tr>
                    @foreach ($item['sub_kategori_penilaian'] as $key_sub_kategori_penilaian => $item_sub_kategori_penilaian)
                        <tr>
                            <td></td>
                            <td>{{$item_sub_kategori_penilaian['sub_kategori_penilaian']}}</td>
                            <td></td>
                            <td><center>{{$item_sub_kategori_penilaian['skor_target']}}</center></td>
                            <td><center>{{$item_sub_kategori_penilaian['skor_capaian']}}</center></td>
                            <td><center>{{$item_sub_kategori_penilaian['presentase_capaian']}}</center></td>
                            <td><center>{{$item_sub_kategori_penilaian['predikat']}}</center></td>
                            <td></td>
                            <td>
                                <center><a href="#" data-toggle="modal" data-id_sub_kategori_dosen="{{$item_sub_kategori_penilaian['id_sub_kategori_dosen']}}" data-target="#CreateNilaiPegawaiModal" class="btn btn-{{$item_sub_kategori_penilaian['button_color_check_nilai']}} btn-sm m-btn  m-btn m-btn--icon btn-block">
                                    <span>
                                        <i class="la la-edit"></i>
                                        <span>{{$item_sub_kategori_penilaian['check_available_penilaian']}}</span>
                                    </span>
                                    </a>
                                </center>
                            </td>
                        </tr>
                    @endforeach
                @endforeach
            </table>
            <br>
            Predikat<br>
            <table style="font-size: 12px">
                <tr>
                    <td>Istimewa</td>
                    <td>:</td>
                    <td>Nilai Capaian >= 120 % Target</td>
                </tr>
                <tr>
                    <td>Sangat Baik</td>
                    <td>:</td>
                    <td>Nilai Capaian >= 110 % - 120 % Target</td>
                </tr>
                <tr>
                    <td>Baik</td>
                    <td>:</td>
                    <td>Nilai Capaian >= 100 % - < 110 % Target</td>
                </tr>
                <tr>
                    <td>Cukup</td>
                    <td>:</td>
                    <td>Nilai Capaian >= 80 % - < 100 % Target</td>
                </tr>
                <tr>
                    <td>Kurang</td>
                    <td>:</td>
                    <td>Nilai Capaian <= 80 % Target</td>
                </tr>
            </table>
        </div>

    </div>
</div>


<div class="modal fade" style="margin-top: -15px" id="CreateNilaiPegawaiModal">
    <div class="modal-dialog modal-lg" style="max-width: 99%;overflow-y: initial !important">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header" style="background-color:#f1f1f1">
                <h4 class="modal-title">Penilaian Evaluasi Pegawai</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                </div>
                <table border="1" class="rwd-table" id="detail_penilaian" style="width: 100%">
                </table>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="SubmitCreateForm">Selesai</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
            </div>
        </div>
    </div>
</div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
    <script src="js_crud/index.js"></script>
<script>

    $(document).ready(function(){

        modal_show();

        function modal_show() {
            $('#CreateNilaiPegawaiModal').on('show.bs.modal', function (event) {

                var button = $(event.relatedTarget)
                var id_sub_kategori_dosen = button.data('id_sub_kategori_dosen');
                $.ajax({
                    type : 'get',
                    url  : '{{url("sub_kategori_penilaian_dosen")}}' + '/' + id_sub_kategori_dosen,
                    dataType:'json',
                    success:function(response){
                        console.log(response);
                        var count = response[0].komponen_kompetensi.length;
                        var isi = '';
                        {{--  var sub_kategori = '<tr><td>'+response[0].sub_kategori+'</td></tr>';  --}}
                        var nomer = 1;
                        var th = '<tr><TH rowspan="2">NO</TH><TH  rowspan="2" style="text-align: center"> Komponen Kompetensi</TH><TH colspan="5" style="text-align: center"> Skor</TH><TH rowspan="2" style="text-align: center"> Skor</TH><TH rowspan="2" style="text-align: center"> Skor/Skor maks</TH><TH rowspan="2" style="text-align: center"> Skor x Bobot sub</TH></tr><tr><TH style="text-align: center">1</TH><TH style="text-align: center">2</TH><TH style="text-align: center">3</TH><TH style="text-align: center">4</TH><TH style="text-align: center">5</TH></tr>';
                        for(var a=0;a<count;a++){
                            isi += '<tr>'+'<td>'+nomer++ +'</td>'+'<td>'+'Deskripsi penilaian'+'</td>'+'<td>'+response[0].komponen_kompetensi[a].skor_1+'<td>'+response[0].komponen_kompetensi[a].skor_2+'</td>'+'<td>'+response[0].komponen_kompetensi[a].skor_3+'</td>'+'<td>'+response[0].komponen_kompetensi[a].skor_4+'</td>'+'<td>'+response[0].komponen_kompetensi[a].skor_5+'</td>'+'<td rowspan="2" style="text-align: center"><div id="skor_nilai_'+response[0].komponen_kompetensi[a].id_komponen_kompetensi+'">0</div></td>'+'<td rowspan="2" style="text-align: center"><div id="skor_nilai_dibagi_maksimal_'+response[0].komponen_kompetensi[a].id_komponen_kompetensi+'">0</div></td>'+'<td rowspan="2" style="text-align: center"><input type="hidden" id="value_bobot_'+response[0].komponen_kompetensi[a].id_komponen_kompetensi+'" value="'+response[0].komponen_kompetensi[a].bobot+'"><div id="perhitungan_bobot_'+response[0].komponen_kompetensi[a].id_komponen_kompetensi+'">0,000</div></td>'+'</tr>'+'<tr>'+'<td>'+''+'</td>'+'<td>'+response[0].komponen_kompetensi[a].judul_komponen_kompetensi+'</td>'+'<td style="text-align: center"><input data-id="'+response[0].komponen_kompetensi[a].id_komponen_kompetensi+'" class="nilai" type="radio"  name="'+response[0].id_kategori_sub+response[0].komponen_kompetensi[a].id_komponen_kompetensi+'" value="1"><input type="hidden"  value="1"></td>'+'<td style="text-align: center"><input data-id="'+response[0].komponen_kompetensi[a].id_komponen_kompetensi+'" class="nilai" type="radio"  name="'+response[0].id_kategori_sub+response[0].komponen_kompetensi[a].id_komponen_kompetensi+'" value="2"><input type="hidden"  value="2"></td>'+'<td style="text-align: center"><input data-id="'+response[0].komponen_kompetensi[a].id_komponen_kompetensi+'" class="nilai" type="radio"  name="'+response[0].id_kategori_sub+response[0].komponen_kompetensi[a].id_komponen_kompetensi+'" value="3"><input type="hidden"  value="3"></td>'+'<td style="text-align: center"><input data-id="'+response[0].komponen_kompetensi[a].id_komponen_kompetensi+'" class="nilai" type="radio"  name="'+response[0].id_kategori_sub+response[0].komponen_kompetensi[a].id_komponen_kompetensi+'" value="4"><input type="hidden"  value="4"></td>'+'<td style="text-align: center"><input data-id="'+response[0].komponen_kompetensi[a].id_komponen_kompetensi+'" class="nilai" type="radio"  name="'+response[0].id_kategori_sub+response[0].komponen_kompetensi[a].id_komponen_kompetensi+'" value="5"><input type="hidden"  value="5"></td>'+'</tr>';
                            $('#detail_penilaian').html(th+isi);
                        }
                        $(".nilai").change(function(){
                            var selValue = $("input[type='radio']:checked").val();
                            var id = $(this).data('id');
                            var value = $(this).val();
                            var skor_nilai = $('#skor_nilai_'+ id).html(value);
                            var bobot = $('#value_bobot_' + id).val();
                            var pembagian_skor = (value / 5);
                            var hitung_bobot = value * (bobot/100);
                            $('#skor_nilai_dibagi_maksimal_' + id).html(pembagian_skor);
                            $('#perhitungan_bobot_' + id).html(hitung_bobot.toFixed(3));

                        });
                    }
                   });

            });
        }



    });

</script>
@endsection
