@extends('master')
@section('konten')
<div class="m-portlet m-portlet--mobile">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											<i class="m-menu__link-icon flaticon-list"></i>&nbsp Master Kategori penilaian (Dosen)
										</h3>
									</div>
								</div>
								<div class="m-portlet__head-tools">
									<ul class="m-portlet__nav">
										<li class="m-portlet__nav-item">
											<a href="#" data-toggle="modal" data-target="#CreateKategoriPenilaianModal" class="btn btn-success m-btn m-btn--icon m-btn--air">
												<span>
													<i class="la la-plus"></i>
													<span>Tambah Kategori</span>
												</span>
                                            </a>

										</li>
										<li class="m-portlet__nav-item"></li>
										<li class="m-portlet__nav-item">
											<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
												<a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
													<i class="la la-ellipsis-h m--font-brand"></i>
												</a>

											</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="m-portlet__body">

								<!--begin: Datatable -->
								<table class="table table-striped- table-bordered table-hover tabel_show" style="width: 100%">
									<thead>
										<tr>
                                            <th>No.</th>
                                            <th>Kategori</th>
                                            <th>Keterangan</th>
                                            <th>Bobot</th>
											<th>Update at</th>
											<th style="width: 40%">Aksi</th>

										</tr>
									</thead>

								</table>
							</div>
						</div>


 <!-- Create Modal -->
 <div class="modal" id="CreateKategoriPenilaianModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Tambah Kategori Penilaian</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                    <strong>Success!</strong>Product was added successfully.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="form-group">
                    <label for="Name">Kategori:</label>
                    <input type="text" class="form-control" placeholder="Kategori" name="kategori" id="tambah_kategori">
                </div>
                <div class="form-group">
                    <label for="Name">Bobot:</label>
                    <input type="text" class="form-control" placeholder="Bobot" name="bobot" id="tambah_bobot">
                </div>
                <div class="form-group">
                    <label for="Name">Keterangan:</label>
                    <input type="text" class="form-control" placeholder="keterangan" name="keterangan" id="tambah_keterangan">
                </div>

            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="SubmitCreateForm">Create</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

       {{--modal edit ikatan kerja--}}
       <div class="modal" id="modal-edit-kategori-penilaian" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color:#f1f1f1">
                    <h5 class="modal-title">Ubah Kategori Penilaian</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form id="form_edit_kategori_penilaian" method="post">

                    <div class="modal-body">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Kategori penilaian:</label>
                            <input type="hidden" name="id" class="form-control" id="txtedit_id">
                            <input type="text" placeholder="Kategori" name="kategori" class="form-control" id="txtedit_kategori" required>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Bobot:</label>
                            <input type="text" placeholder="Bobot" name="bobot" class="form-control" id="txtedit_bobot" required>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Keterangan:</label>
                            <input type="text" placeholder="Keterangan" name="keterangan" class="form-control" id="txtedit_keterangan"
                                   >
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success btn-flat">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

     {{--modal sub kategori penilaian--}}
     <div class="modal fade" id="modal-sub-kategori-penilaian" role="dialog">
        <div class="modal-dialog  modal-lg" style="max-width: 95%;overflow-y: initial !important">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Sub Kategori</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form id="form_edit_kategori_penilaian" method="post">
                    <div class="modal-body" style="height: 390px;
                    overflow-y: auto;">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-md-6">
                                <table>
                                    <tr>
                                        <td>
                                        Kategori Penilaian
                                        </td>
                                        <td>&nbsp&nbsp:&nbsp&nbsp</td>

                                        <td><span class="m--font-boldest" ><div id="kategori_penilaian"></div></span></td>
                                    </tr>
                                    <tr>
                                        <td>
                                        Bobot
                                        </td>
                                        <td>&nbsp&nbsp:&nbsp&nbsp</td>
                                        <td> <span class="m--font-boldest" ><div id="kategori_penilaian_bobot"></div></span></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Keterangan
                                        </td>
                                        <td>&nbsp&nbsp:&nbsp&nbsp</td>
                                        <td> <span class="m--font-boldest" ><div id="kategori_penilaian_keterangan"></div></span></td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-6"><h5>Sub Kategori Penilaian</h5></div>
                                    <div class="col-md-6 text-right"><a href="#" data-toggle="modal" data-target="#CreateSubKategoriPenilaianModal" class="btn btn-success m-btn m-btn--icon m-btn--air">
                                        <span>
                                            <i class="la la-plus"></i>
                                            <span>Tambah Kategori</span>
                                        </span>
                                    </a></div>
                                </div>

                                {{--  <input type="text" id="txtedit_id_sub_kategori">  --}}
                                <hr style="width: 100%">
                                    <br>


                                <table class="table table-striped- table-bordered table-hover tabel_sub_kategori_show" style="width: 100%">
									<thead>
										<tr>
                                            <th>No.</th>
                                            <th>Kategori</th>
											<th>Update at</th>
											<th style="width: 20%">Aksi</th>

										</tr>
									</thead>

								</table>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success btn-flat">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Create Modal -->
 <div class="modal fade" id="CreateSubKategoriPenilaianModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Tambah Kategori Penilaian</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                    <strong>Success!</strong>Product was added successfully.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="form-group">
                    <label for="Name">Kategori:</label>
                    <input type="text" class="form-control" placeholder="Kategori" name="kategori" id="tambah_kategori">
                </div>
                <div class="form-group">
                    <label for="Name">Keterangan:</label>
                    <input type="text" class="form-control" placeholder="keterangan" name="keterangan" id="tambah_keterangan">
                </div>

            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="SubmitCreateForm">Create</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
    <script src="js_crud/index.js"></script>
<script>

    $(document).ready(function(){

        var table_kategori_penilaian = $('.tabel_show').DataTable({
            responsive:true,
            paging: true,
            info: true,
            searching: true,
            "aaSorting": [],
            "ordering": true,
            ajax: {
                url: '{{url("datatable_kategori_penilaian")}}',
                dataSrc: 'result',
            },

        });



        $('#SubmitCreateForm').click(function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{ url('simpan_master_kategori_penilaian') }}",
                method: 'post',
                data: {
                    kategori: $('#tambah_kategori').val(),
                    bobot: $('#tambah_bobot').val(),
                    keterangan: $('#tambah_keterangan').val(),

                },
                success: function(result) {
                    if(result.errors) {
                        $('.alert-danger').html('');
                        $.each(result.errors, function(key, value) {
                            $('.alert-danger').show();
                            $('.alert-danger').append('<strong><li>'+value+'</li></strong>');
                        });
                    } else {
                        $('.alert-danger').hide();
                        $('.alert-success').show();
                        swal.fire({
                            title: "Information!",
                            html: '<strong>' + result.success + '</strong>',
                            type:'success',
                            button: false,
                            timer: 2000,
                        })
                        $('.tabel_show').DataTable().ajax.reload();
                            $('.alert-success').hide();
                            $('#CreateKategoriPenilaianModal').modal('hide');


                    }
                }
            });
        });

        $('#form_edit_kategori_penilaian').submit(function (event) {
            $('#modal-edit-kategori-penilaian').modal('hide');
            event.preventDefault();
            var url = '{{ url('update_master_kategori_penilaian') }}';
            ajaxProcess(url, $(this).serialize())
        });

        function ajaxProcess(url, data) {
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                beforeSend: function () {
                    $('.cssload-container').show();
                },
                success: function (response) {
                    if (response.status == 'success') {
                        $('.cssload-container').hide();

                        $('.tabel_show').DataTable().ajax.reload();


                        Swal.fire({
                            title: 'Information',
                            html: '<strong>' + response.result + '</strong>',
                            type: 'success',
                            showConfirmButton: false,
                            timer: 3000
                        });
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error);
                        var result = error.responseJSON;
                        if (result != null) {
                            var message = result.message;
                            if (Array.isArray(message)) {
                                $.map(message, function (value, index) {
                                    message = value + '</br>';
                                });
                            }
                        } else {
                            message = 'look like something when wrong';
                        }
                    } else {
                        message = 'look like something when wrong';
                    }


                    $('#form_edit_kategori_penilaian')[0].reset();
                    $('.cssload-container').hide();
                    Swal.fire({
                        title: 'Warning',
                        html: '<strong>' + message + '</strong>',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 3000
                    })

                }

            })
        };


        //show detail data
        modal_edit_kategori_penilaian();

        function modal_edit_kategori_penilaian() {
            $('#modal-edit-kategori-penilaian').on('show.bs.modal', function (event) {
                // do something...
                var button = $(event.relatedTarget) // Button that triggered the modal
                var id = button.data('id');
                var kategori = button.data('kategori');
                var bobot = button.data('bobot');
                var persen_bobot = bobot * 100;
                var keterangan = button.data('keterangan');
                $('#txtedit_id').val(id);
                $('#txtedit_kategori').val(kategori);
                $('#txtedit_bobot').val(persen_bobot);
                $('#txtedit_keterangan').val(keterangan);
            });
        }

        //show kategori sub modal
        modal_sub_kategori_penilaian();

        function modal_sub_kategori_penilaian() {
            $('#modal-sub-kategori-penilaian').on('show.bs.modal', function (event) {
                // do something...
                var button = $(event.relatedTarget) // Button that triggered the modal
                var id = button.data('id');
                var kategori = button.data('kategori');
                var bobot = button.data('bobot');
                var convert_bobot = (bobot * 100) + '%';
                var keterangan = button.data('keterangan');
                $('#txtedit_id_sub_kategori').val(id);
                $('#kategori_penilaian').html(kategori);
                $('#kategori_penilaian_keterangan').html(keterangan);
                $('#kategori_penilaian_bobot').html(convert_bobot);
                var table_sub_kategori_penilaian = $('.tabel_sub_kategori_show').DataTable({
                    responsive:true,
                    destroy: true,
                    paging: true,
                    info: true,
                    searching: true,
                    "aaSorting": [],
                    "ordering": true,
                    ajax: {
                        url: '{{url("datatable_sub_kategori_penilaian")}}' + '/' + id,
                        dataSrc: 'result',
                    },

                });
            });
        }



       $('.tabel_show tbody').on('click', '#btn-delete-kategori-penilaian', function () {
        var url = '{{ url('destroy_master_kategori_penilaian') }}';
        var id = $(this).attr('data-id');
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var kategori_penilaian = {_token: CSRF_TOKEN, id: id};
        delete_master(url, kategori_penilaian);
        });

    });

</script>
@endsection

