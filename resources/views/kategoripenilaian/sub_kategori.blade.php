@extends('master_sub')
@section('konten')

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <i class="m-menu__link-icon flaticon-list"></i>&nbsp Sub Kategori penilaian (Dosen)
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="#" data-toggle="modal" data-target="#CreateKategoriPenilaianModal" class="btn btn-success m-btn m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Tambah Sub Kategori</span>
                        </span>
                    </a>

                </li>
                <li class="m-portlet__nav-item"></li>
                <li class="m-portlet__nav-item">
                    <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                        <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                            <i class="la la-ellipsis-h m--font-brand"></i>
                        </a>

                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">

        <!--begin: Datatable -->
        <form id="form_edit_kategori_penilaian" method="post">

            {!! csrf_field() !!}
            <div class="row">
                <div class="col-md-6">
                    <table>
                        <tr>
                            <td>
                            Kategori Penilaian
                            </td>
                            <td>&nbsp&nbsp:&nbsp&nbsp</td>

                            <td><span class="m--font-boldest" >{{$query->kategori}}</span></td>
                        </tr>
                        <tr>
                            <td>
                            Bobot
                            </td>
                            <td>&nbsp&nbsp:&nbsp&nbsp</td>
                            <td> <span class="m--font-boldest" >{{($query->bobot * 100) }} %</span></td>
                        </tr>
                        <tr>
                            <td>
                                Keterangan
                            </td>
                            <td>&nbsp&nbsp:&nbsp&nbsp</td>
                            <td> <span class="m--font-boldest" >{{$query->keterangan}}</span></td>
                        </tr>
                    </table>
                </div>

            </div>
                    <br>

                    {{--  <input type="text" id="txtedit_id_sub_kategori">  --}}
                    <hr style="width: 100%">
                        <br>


                    <table class="table table-striped- table-bordered table-hover tabel_sub_kategori_show" style="width: 100%">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Kategori</th>
                                <th style="width: 15%">Update at</th>
                                <th style="width: 40%">Aksi</th>

                            </tr>
                        </thead>

                    </table>

    </form>
    </div>
</div>

 {{--modal sub kategori penilaian--}}
 <div class="modal fade" id="modal-komponen-kompetensi" role="dialog">
    <div class="modal-dialog  modal-lg" style="max-width: 95%;overflow-y: initial !important">
        <div class="modal-content">
            <div class="modal-header" style="background-color:darkseagreen">
                <span>
                    <i class="la la-list"></i>
                 <span style="color: white;font-size:15px">Komponen Kompetensi</span>
                </span>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form id="form_edit_kategori_penilaian" method="post">
                <div class="modal-body" style="height: 390px;
                overflow-y: auto;">
                    {!! csrf_field() !!}
                            <div class="row">
                                <div class="col-md-6"><h5><div id="sub_kategori"></div></h5></div>
                                <div class="col-md-6 text-right"><a href="#" data-toggle="modal" data-target="#CreateSubKategoriPenilaianModal" class="btn btn-success m-btn m-btn--icon m-btn--air">
                                    <span>
                                        <i class="la la-plus"></i>
                                        <span>Tambah Kategori</span>
                                    </span>
                                </a></div>
                            </div>

                            {{--  <input type="text" id="txtedit_id_sub_kategori">  --}}
                            <hr style="width: 100%">
                                <br>


                            <table class="table display table-striped- table-bordered table-hover tabel_komponen_kompetensi" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th style="width: 3%">No.</th>
                                        <th style="width: 30%">Kompetensi</th>
                                        <th style="width: 10%">Des Skor 1</th>
                                        <th style="width: 10%">Des Skor 2</th>
                                        <th style="width: 10%">Des Skor 3</th>
                                        <th style="width: 10%">Des Skor 4</th>
                                        <th style="width: 20%">Des Skor 5</th>
                                        <th style="width: 10%">Aksi</th>

                                    </tr>
                                </thead>

                            </table>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-flat" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success btn-flat">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Create Modal -->
<div class="modal fade" id="CreateKategoriPenilaianModal">
   <div class="modal-dialog">
       <div class="modal-content">
           <!-- Modal Header -->
           <div class="modal-header">
               <h4 class="modal-title">Tambah Kategori Penilaian</h4>
               <button type="button" class="close" data-dismiss="modal">&times;</button>
           </div>
           <!-- Modal body -->
           <div class="modal-body">
               <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                       <span aria-hidden="true">&times;</span>
                   </button>
               </div>
               <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                   <strong>Success!</strong>Product was added successfully.
                   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                       <span aria-hidden="true">&times;</span>
                   </button>
               </div>

                <input type="hidden" class="form-control" placeholder="keterangan" value="{{ Request::segment(2) }}" name="kategori_penilaian_id" id="tambah_kategori_penilaian_id">

               <div class="form-group">
                   <label for="Name">Sub Kategori:</label>
                   <input type="text" class="form-control" placeholder="Sub Kategori" name="sub_kategori" id="tambah_sub_kategori">
               </div>


           </div>
           <!-- Modal footer -->
           <div class="modal-footer">
               <button type="button" class="btn btn-success" id="SubmitCreateForm">Create</button>
               <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
           </div>
       </div>
   </div>
</div>

 {{--modal edit ikatan kerja--}}
 <div class="modal" id="modal-edit-sub-kategori-penilaian" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Ubah Sub Kategori</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form id="form_edit_sub_kategori" method="post">
                <div class="modal-body">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Sub Kategori:</label>
                        <input type="hidden" name="id" class="form-control" id="txtedit_id">
                        <input type="text" placeholder="sub Kategori" name="sub_kategori" class="form-control" id="txtedit_sub_kategori" required>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-flat" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success btn-flat">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script src="../js_crud/index.js"></script>

<script>

$(document).ready(function(){
    var id = "{{Request::segment(2)}}";
    console.log(id);
    var table_sub_kategori_penilaian = $('.tabel_sub_kategori_show').DataTable({
        responsive:true,
        destroy: true,
        paging: true,
        info: true,
        responsive: true,
        searching: true,
        "aaSorting": [],
        "ordering": true,
        ajax: {
            url: '{{url("datatable_sub_kategori_penilaian")}}' + '/' + id,
            dataSrc: 'result',
        },

    });

    modal_komponen_kompetensi_penilaian();

        function modal_komponen_kompetensi_penilaian() {
            $('#modal-komponen-kompetensi').on('show.bs.modal', function (event) {
                // do something...
                var button = $(event.relatedTarget) // Button that triggered the modal
                var id = button.data('id');
                var sub_kategori = button.data('sub_kategori');
                $('#sub_kategori').html(sub_kategori);
                var table_komponen_kompetensi = $('.tabel_komponen_kompetensi').DataTable({
                    responsive:true,
                    destroy: true,
                    paging: true,
                    info: true,
                    responsive: true,
                    searching: true,
                    "aaSorting": [],
                    "ordering": true,
                    ajax: {
                        url: '{{url("datatable_komponen_kompetensi")}}' + '/' + id,
                        dataSrc: 'result',
                    },

                });
            });
        }

        modal_sub_kategori_penilaian();

        function modal_sub_kategori_penilaian() {
            $('#modal-edit-sub-kategori-penilaian').on('show.bs.modal', function (event) {
                // do something...
                var button = $(event.relatedTarget) // Button that triggered the modal
                var id = button.data('id');
                var sub_kategori = button.data('sub_kategori');
                $('#txtedit_sub_kategori').val(sub_kategori);
                $('#txtedit_id').val(id);

            });
        }

        $('#SubmitCreateForm').click(function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{ url('simpan_sub_kategori_penilaian_dosen') }}",
                method: 'post',
                data: {
                    kategori_penilaian_id: $('#tambah_kategori_penilaian_id').val(),
                    sub_kategori: $('#tambah_sub_kategori').val(),

                },
                success: function(result) {
                    if(result.errors) {
                        $('.alert-danger').html('');
                        $.each(result.errors, function(key, value) {
                            $('.alert-danger').show();
                            $('.alert-danger').append('<strong><li>'+value+'</li></strong>');
                        });
                    } else {
                        $('.alert-danger').hide();
                        $('.alert-success').show();
                        swal.fire({
                            title: "Information!",
                            html: '<strong>' + result.success + '</strong>',
                            type:'success',
                            button: false,
                            timer: 2000,
                        })
                        $('.tabel_sub_kategori_show').DataTable().ajax.reload();
                            $('.alert-success').hide();
                            $('#CreateKategoriPenilaianModal').modal('hide');


                    }
                }
            });
        });

        $('#form_edit_sub_kategori').submit(function (event) {
            $('#modal-edit-sub-kategori-penilaian').modal('hide');
            event.preventDefault();
            var url = '{{ url('update_sub_kategori_penilian_dosen') }}';
            ajaxProcess(url, $(this).serialize())
        });

        function ajaxProcess(url, data) {
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                beforeSend: function () {
                    $('.cssload-container').show();
                },
                success: function (response) {
                    if (response.status == 'success') {
                        $('.cssload-container').hide();

                        $('.tabel_sub_kategori_show').DataTable().ajax.reload();
                        Swal.fire({
                            title: 'Information',
                            html: '<strong>' + response.result + '</strong>',
                            type: 'success',
                            showConfirmButton: false,
                            timer: 3000
                        });
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error);
                        var result = error.responseJSON;
                        if (result != null) {
                            var message = result.message;
                            if (Array.isArray(message)) {
                                $.map(message, function (value, index) {
                                    message = value + '</br>';
                                });
                            }
                        } else {
                            message = 'look like something when wrong';
                        }
                    } else {
                        message = 'look like something when wrong';
                    }


                    $('#form_edit_sub_kategori')[0].reset();
                    $('.cssload-container').hide();
                    Swal.fire({
                        title: 'Warning',
                        html: '<strong>' + message + '</strong>',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 3000
                    })

                }

            })
        };

        $('.tabel_sub_kategori_show tbody').on('click', '#btn-delete-sub-kategori-penilaian', function () {
            var url = '{{ url('destroy_sub_kategori_penilian_dosen') }}';
            var id = $(this).attr('data-id');
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var sub_kategori_penilaian = {_token: CSRF_TOKEN, id: id};
            delete_master(url, sub_kategori_penilaian);

            });

});
</script>
@endsection
