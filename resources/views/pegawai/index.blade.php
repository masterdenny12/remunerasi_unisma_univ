@extends('master')
@section('konten')
<div class="m-portlet m-portlet--mobile">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											<i class="m-menu__link-icon flaticon-users"></i>&nbsp Data Pegawai
										</h3>
									</div>
								</div>
								<div class="m-portlet__head-tools">
									<ul class="m-portlet__nav">
										<li class="m-portlet__nav-item">
											<a href="#" data-toggle="modal" data-target="#CreatePegawaiModal" class="btn btn-success m-btn m-btn--icon m-btn--air">
												<span>
													<i class="la la-plus"></i>
													<span>Tambah Data</span>
												</span>
											</a>
										</li>
										<li class="m-portlet__nav-item"></li>
										<li class="m-portlet__nav-item">
											<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
												<a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
													<i class="la la-ellipsis-h m--font-brand"></i>
												</a>

											</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="m-portlet__body">

								<!--begin: Datatable -->
								<table class="table table-striped- table-bordered table-hover tabel_show" >
									<thead>
										<tr>
                                            <th>No.</th>
											<th>NIP</th>
											<th>Nama</th>
                                            <th>Status</th>
											<th>Update at</th>
											<th>Aksi</th>

										</tr>
									</thead>

								</table>
							</div>
						</div>


 <!-- Create Modal -->
    <div class="modal fade" id="CreatePegawaiModal">
            <div class="modal-dialog modal-lg" style="max-width: 97%;overflow-y: initial !important">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header" style="background-color: #efefef">
                        <h4 class="modal-title">Tambah Pegawai</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body" style="height: 400px;
                    overflow-y: auto;">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                            <strong>Success!</strong>Product was added successfully.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <form id="form_submit" method="post">
                                        @csrf
                                    {{-- {{dd(url('images/'.$data->photo))}} --}}
                                    <img src="{{asset('img-preview.jpg')}}" id="profile-img-tag" class="foto_pegawai" width="140px" height="140px style="margin-bottom: 5px;margin-left:20px"/>
                                    <label style="width: 140px" class="btn btn-success btn-flat btn-block">
                                        <i class="fa fa-upload"></i>&nbsp Upload Foto
                                        <input type="file" name="foto" id="foto_pegawai"
                                            accept="image/PNG, image/png, image/jpg,image/JPG, image/jpeg, image/JPEG"
                                            class="uploadFile img photo"
                                            style="width: 0px;height: 5px;">
                                    </label>

                                    <p id="error_photo1" style="display:none; color:#FF0000;">
                                        Invalid Photo Format! Format Must Be JPG,PNG or jpeg.
                                        </p>
                                        <p id="error_photo2" style="display:none; color:#FF0000;">
                                        Maximum File Size Limit is 1MB.
                                    </p>

                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">NIP:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" placeholder="NIP" name="nip" id="tambah_nip">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Nama:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" placeholder="ex.denny kharisma" name="nama_pegawai" id="tambah_nama_pegawai">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">NO HP:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" placeholder="No telp" name="no_telp" id="tambah_no_hp">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Tanggal lahir:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="input-group m-input-group m-input-group--square">

                                                <input type="text" name="tanggal_lahir" placeholder="Tanggal lahir" class="form-control"
                                                    id="tanggal_lahir"  autocomplete="off">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Jenis kelamin:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1" value="L" name="jenis_kelamin" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">Laki-laki</label>
                                              </div>
                                              <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio2" value="P" name="jenis_kelamin" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio2">Perempuan</label>
                                              </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Alamat:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" placeholder="Alamat" name="alamat" id="tambah_alamat">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Kota:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="kota_id" id="kota_asal"
                                            class="custom-select select2 form-control" style="width:100%">
                                            </select>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Pekerjaan:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="kategori_pegawai_id" id="kategori_pegawai"
                                            class="custom-select select2 form-control" style="width:100%">
                                            </select>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <br>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Tanggal masuk:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="input-group m-input-group m-input-group--square">

                                                <input type="text" name="tanggal_masuk" placeholder="Tanggal masuk" class="form-control"
                                                    id="tanggal_masuk" autocomplete="off">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Status Perkawinan:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="status_perkawinan_id" id="status_perkawinan_id"
                                            class="custom-select select2 form-control" style="width:100%">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Ikatan kerja:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="status_ikatan_kerja_id" id="status_ikatan_kerja_id"
                                                class="custom-select select2 form-control" style="width:100%">

                                                </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Status pegawai:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1statuspegawai" name="status_pegawai" value="aktif" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1statuspegawai">Aktif</label>
                                              </div>
                                              <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio2statuspegawai" name="status_pegawai" value="tidak aktif" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio2statuspegawai">Tidak aktif</label>
                                              </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Jenjang Pendidikan:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="pendidikan_terakhir_id" id="jenjang_pendidikan"
                                                class="custom-select select2 form-control" style="width:100%">

                                                </select>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>



                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" id="SubmitCreateForm">Create</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </form>
                </div>
            </div>
        </div>

             {{-- modal edit pegawai --}}
    <div class="modal fade" id="modal-edit-pegawai" role="dialog">
        <div class="modal-dialog modal-lg" style="max-width: 97%;overflow-y: initial !important">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #efefef">
                    <h4 class="modal-title">Ubah Pegawai</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form id="form_edit" enctype="multipart/form-data" method="post">
                    {!! csrf_field() !!}
                    <div class="modal-body" style="height: 400px;
                    overflow-y: auto;">

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    {{-- {{dd(url('images/'.$data->photo))}} --}}
                                    <img id="profile-img-tag-edit" class="foto_pegawai_edit" width="140px" height="140px style="margin-bottom: 5px;margin-left:20px"/>
                                    <label style="width: 140px" class="btn btn-success btn-flat btn-block">
                                        <i class="fa fa-upload"></i>&nbsp Upload Foto
                                        <input type="file" name="foto_edit" id="foto_pegawai_edit"
                                            accept="image/PNG, image/png, image/jpg,image/JPG, image/jpeg, image/JPEG"
                                            class="uploadFile img photo"
                                            style="width: 0px;height: 5px;">
                                    </label>

                                    <p id="error_photo1" style="display:none; color:#FF0000;">
                                        Invalid Photo Format! Format Must Be JPG,PNG or jpeg.
                                        </p>
                                        <p id="error_photo2" style="display:none; color:#FF0000;">
                                        Maximum File Size Limit is 1MB.
                                    </p>

                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">NIP:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="hidden" class="form-control" placeholder="NIP" name="nip" id="txtedit_id">
                                            <input type="text" class="form-control" placeholder="NIP" name="nip" id="txtedit_nip">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Nama:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" placeholder="ex.denny kharisma" name="nama_pegawai" id="txtedit_nama_pegawai">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">NO HP:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" placeholder="No telp" name="no_telp" id="txtedit_no_telp">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Tanggal lahir:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="input-group m-input-group m-input-group--square">

                                                <input type="text" name="tanggal_lahir" id="txtedit_tanggal_lahir" placeholder="Tanggal lahir" class="form-control"
                                                     autocomplete="off">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Jenis kelamin:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1edit" value="L" name="jenis_kelamin" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1edit">Laki-laki</label>
                                              </div>
                                              <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio2edit" value="P" name="jenis_kelamin" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio2edit">Perempuan</label>
                                              </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Alamat:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" placeholder="Alamat" name="alamat" id="txtedit_alamat">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Kota:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="kota_id" id="kota_asal_edit"
                                            class="custom-select select2 form-control" style="width:100%">
                                            </select>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Pekerjaan:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="kategori_pegawai_id" id="txtedit_kategori_pegawai"
                                                class="custom-select select2 form-control" style="width:100%">

                                                </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Tanggal masuk:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="input-group m-input-group m-input-group--square">

                                                <input type="text" name="tanggal_masuk" placeholder="Tanggal masuk" class="form-control"
                                                    id="txtedit_tanggal_masuk" autocomplete="off">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Status Perkawinan:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="status_perkawinan_id" id="status_perkawinan_id_edit"
                                            class="custom-select select2 form-control" style="width:100%">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Ikatan kerja:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="status_ikatan_kerja_id" id="status_ikatan_kerja_id_edit"
                                                class="custom-select select2 form-control" style="width:100%">

                                                </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Status pegawai:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1statuspegawaiedit" name="status_pegawai" value="aktif" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1statuspegawaiedit">Aktif</label>
                                              </div>
                                              <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio2statuspegawaiedit" name="status_pegawai" value="tidak aktif" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio2statuspegawaiedit">Tidak aktif</label>
                                              </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">Jenjang Pendidikan:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="pendidikan_terakhir_id" id="txtedit_jenjang_pendidikan"
                                                class="custom-select select2 form-control" style="width:100%">

                                                </select>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success btn-flat">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
    <script src="js_crud/index.js"></script>
<script>

    $(document).ready(function(){

        var table_employee = $('.tabel_show').DataTable({
            responsive:true,
            paging: true,
            info: true,
            searching: true,
            "aaSorting": [],
            "ordering": true,
            ajax: {
                url: '{{url("datatable_pegawai")}}',
                dataSrc: 'result',
            },

        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#profile-img-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#foto_pegawai").change(function(){
            readURL(this);
        });

        function readURL_edit(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#profile-img-tag-edit').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#foto_pegawai_edit").change(function(){
            readURL_edit(this);
        });
        //tanggal lahir
        $('#tanggal_lahir').datepicker({
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true,
            format: 'dd-mm-yyyy'
        });
        $('#txtedit_tanggal_lahir').datepicker({
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true,
            format: 'dd-mm-yyyy'
        });
        //tanggal masuk
        $('#tanggal_masuk').datepicker({
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true,
            format: 'dd-mm-yyyy'
        });
        //tanggal masuk edit
        $('#txtedit_tanggal_masuk').datepicker({
            orientation: "bottom auto",
            autoclose: true,
            todayHighlight: true,
            format: 'dd-mm-yyyy'
        });
        //jenis kelamin
        $('.jenis_kelamin').select2({
            dropdownParent:$('.jenis_kelamin').parent(),
            placeholder: 'Pilih jenis kelamin',

        });
          //status pegawai
          $('#status_pegawai').select2({
            dropdownParent:$('#status_pegawai').parent(),
            placeholder: 'Pilih status pegawai',

        });
         //status pegawai
         $('#jenis_kelamin_edit').select2({
            dropdownParent:$('#jenis_kelamin_edit').parent(),
            placeholder: 'Pilih jenis kelamin',

        });
        //status perkawinan
        $('#status_perkawinan_id').select2({
            placeholder: 'Status Perkawinan...',
            dropdownParent: $("#status_perkawinan_id").parent(),
            ajax: {
              url: '{{url("/cari/status_perkawinan")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.status_perkawinan,
                      id: item.id
                    }
                  })
                };
              },
              cache: true,
            }
          });

          //status perkawinan edit
        $('#status_perkawinan_id_edit').select2({
            placeholder: 'Status Perkawinan...',
            dropdownParent: $("#status_perkawinan_id_edit").parent(),
            ajax: {
              url: '{{url("/cari/status_perkawinan")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.status_perkawinan,
                      id: item.id
                    }
                  })
                };
              },
              cache: true,
            }
          });
        //kota
        $('#kota_asal').select2({
            placeholder: 'Kota asal...',
            dropdownParent: $("#kota_asal").parent(),
            ajax: {
              url: '{{url("/cari/kota")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.nama_kota,
                      id: item.id
                    }
                  })
                };
              },
              cache: true,
            }
          });
          //kota
        $('#kota_asal_edit').select2({
            placeholder: 'Kota asal...',
            dropdownParent: $("#kota_asal_edit").parent(),
            ajax: {
              url: '{{url("/cari/kota")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.nama_kota,
                      id: item.id
                    }
                  })
                };
              },
              cache: true,
            }
          });
          //ikatan kerja
          $('#status_ikatan_kerja_id').select2({
            placeholder: 'Ikatan kerja...',
            dropdownParent: $("#status_ikatan_kerja_id").parent(),
            ajax: {
              url: '{{url("/cari/ikatan_kerja")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.ikatan_kerja,
                      id: item.id
                    }
                  })
                };
              },
              cache: true,
            }
          });

          //ikatan kerja edit
          $('#status_ikatan_kerja_id_edit').select2({
            placeholder: 'Ikatan kerja...',
            dropdownParent: $("#status_ikatan_kerja_id_edit").parent(),
            ajax: {
              url: '{{url("/cari/ikatan_kerja")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.ikatan_kerja,
                      id: item.id
                    }
                  })
                };
              },
              cache: true,
            }
          });

          //jenjang pendidikan
        $('#jenjang_pendidikan').select2({
            placeholder: 'jenjang pendidikan...',
            dropdownParent: $("#jenjang_pendidikan").parent(),
            ajax: {
              url: '{{url("/cari/jenjang_pendidikan")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.nm_pendidikan,
                      id: item.id
                    }
                  })
                };
              },
              cache: true,
            }
          });

           //jenjang pendidikan
        $('#txtedit_jenjang_pendidikan').select2({
            placeholder: 'jenjang pendidikan...',
            dropdownParent: $("#txtedit_jenjang_pendidikan").parent(),
            ajax: {
              url: '{{url("/cari/jenjang_pendidikan")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.nm_pendidikan,
                      id: item.id
                    }
                  })
                };
              },
              cache: true,
            }
          });

          //kategori pegawai
        $('#kategori_pegawai').select2({
            placeholder: 'Pekerjaan...',
            dropdownParent: $("#kategori_pegawai").parent(),
            ajax: {
              url: '{{url("/cari/jenis_pegawai")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.jenis_pegawai,
                      id: item.id
                    }
                  })
                };
              },
              cache: true,
            }
          });

          //kategori pegawai edit
        $('#txtedit_kategori_pegawai').select2({
            placeholder: 'Pekerjaan...',
            dropdownParent: $("#txtedit_kategori_pegawai").parent(),
            ajax: {
              url: '{{url("/cari/jenis_pegawai")}}',
              dataType: 'json',
              delay: 50,
              processResults: function (data) {
                return {
                  results:  $.map(data, function (item) {
                    return {
                      text: item.jenis_pegawai,
                      id: item.id
                    }
                  })
                };
              },
              cache: true,
            }
          });

        {{-- $('#SubmitCreateForm').click(function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{ url('simpan_data_pegawai') }}",
                method: 'post',
                data: {
                    nip: $('#tambah_nip').val(),
                    nama_pegawai: $('#tambah_nama_pegawai').val(),

                },
                success: function(result) {
                    if(result.errors) {
                        $('.alert-danger').html('');
                        $.each(result.errors, function(key, value) {
                            $('.alert-danger').show();
                            $('.alert-danger').append('<strong><li>'+value+'</li></strong>');
                        });
                    } else {
                        $('.alert-danger').hide();
                        $('.alert-success').show();
                        swal.fire({
                            title: "Information!",
                            html: '<strong>' + result.success + '</strong>',
                            type:'success',
                            button: false,
                            timer: 2000,
                        })
                        $('.tabel_show').DataTable().ajax.reload();
                            $('.alert-success').hide();
                            $('#CreatePegawaiModal').modal('hide');


                    }
                }
            });
        }); --}}

        $('#form_submit').submit(function (event) {
            event.preventDefault();
            $('#CreatePegawaiModal').modal('hide');
            var url = '{{ url('simpan_data_pegawai') }}';
            ajaxProcess(url, new FormData(this))
        });

        function ajaxProcess(url, data) {
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                cache: false,
                processData:false,
                contentType: false,
                beforeSend: function () {
                    $('.cssload-container').show();
                },
                success: function (response) {
                    if (response) {
                        console.log(response);
                        $('.cssload-container').hide();
                        $('#form_submit')[0].reset();
                        $('.tabel_show').DataTable().ajax.reload();
                        Swal.fire({
                            title: 'Information',
                            html: 'Data berhasil disimpan',
                            type: 'success',
                            showConfirmButton: false,
                            timer: 3000
                        });
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error);
                        var result = error.responseJSON;
                        if (result != null) {
                            var message = result.errors;
                            if (Array.isArray(message)) {
                                console.log(message);
                                $.map(message, function (value, index) {
                                    message = value + '</br>';
                                });
                            }
                        } else {
                            message = 'look like something when wrong';
                        }
                    } else {
                        message = 'look like something when wrong';
                    }

                    $('.cssload-container').hide();
                    Swal.fire({
                        title: 'Warning',
                        html: '<strong>' + message + '</strong>',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 3000
                    })

                }

            })
        };


        $('#form_edit').submit(function (event) {
            $('#modal-edit-pegawai').modal('hide');
            event.preventDefault();
            var url = '{{ url('update_data_pegawai') }}';
            ajaxProcess(url, new FormData(this));
        });

        function ajaxProcess(url, data) {
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                cache: false,
                processData:false,
                contentType: false,
                beforeSend: function () {
                    $('.cssload-container').show();
                },
                success: function (response) {
                    if (response.status == 'success') {
                        console.log(response);
                        $('.cssload-container').hide();
                        $('#form_edit')[0].reset();
                        $('.tabel_show').DataTable().ajax.reload();
                        Swal.fire({
                            title: 'Information',
                            html: '<strong>' + response.result + '</strong>',
                            type: 'success',
                            showConfirmButton: false,
                            timer: 3000
                        });
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error);
                        var result = error.responseJSON;
                        if (result != null) {
                            var message = result.message;
                            if (Array.isArray(message)) {
                                console.log(message);
                                $.map(message, function (value, index) {
                                    message = value + '</br>';
                                });
                            }
                        } else {
                            message = 'look like something when wrong';
                        }
                    } else {
                        message = 'look like something when wrong';
                    }

                    $('.cssload-container').hide();
                    Swal.fire({
                        title: 'Warning',
                        html: '<strong>' + message + '</strong>',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 3000
                    })

                }

            })
        };

        $('.tabel_show tbody').on('click', '#btn_detail_emp', function (e) {
            var emp_id = $(this).attr('data-emp_id');
            var url = '{!! url('ajax/hris/emp/detail') !!}/'+emp_id;

            ajaxEditEmployee(url);
        });

        function ajaxEditEmployee(url){
            $.ajax({
                type: 'GET',
                url: url,
                cache: false,
                processData:false,
                contentType: false,
                beforeSend: function () {
                    $('.cssload-container').show();
                },
                success: function (response) {
                    $('.cssload-container').hide();
                    if(response.status == 'success') {
                       if(response.result != null){
                           var data = response.result;
                           $('#txtedit_nip').val(data.nip);
                           $('#txtedit_nama_pegawai').val(data.nama_pegawai);

                           $('#txtedit_no_telp').val(data.no_telp);

                           $('#txtedit_tanggal_lahir').datepicker('setDate', data.tanggal_lahir);

                           if(data.jenis_kelamin == "L"){
                            $('#customRadio1edit').prop("checked", true);
                           }else{
                            $('#customRadio2edit').prop("checked", true);
                           }
                           if(data.foto != null){
                            $('#profile-img-tag-edit').attr('src', '{{asset('foto')}}/'+data.foto);
                           }else{
                               $('#profile-img-tag-edit').attr('src', ' {{asset('img-preview.jpg')}}');

                           }
                           $('#txtedit_alamat').val(data.alamat);

                           var selected_kota = $("<option selected='selected'></option>").val(data.kota_id).text(data.nama_kota);
                           $("#kota_asal_edit").append(selected_kota).trigger('change');

                           $('#txtedit_tanggal_masuk').datepicker('setDate', data.tgl_mulai_masuk);

                           var selected_status_perkawinan = $("<option selected='selected'></option>").val(data.status_perkawinan_id).text(data.status_perkawinan);
                           $("#status_perkawinan_id_edit").append(selected_status_perkawinan).trigger('change');

                           var selected_status_ikatan_kerja = $("<option selected='selected'></option>").val(data.status_ikatan_kerja_id).text(data.ikatan_kerja);
                           $("#status_ikatan_kerja_id_edit").append(selected_status_ikatan_kerja).trigger('change');

                           if(data.status_pegawai == "aktif"){
                            $('#customRadio1statuspegawaiedit').prop("checked", true);
                           }else{
                            $('#customRadio2statuspegawaiedit').prop("checked", true);
                           }

                           var selected_kategori_pegawai = $("<option selected='selected'></option>").val(data.kategori_pegawai_id).text(data.jenis_pegawai);
                           $("#txtedit_kategori_pegawai").append(selected_kategori_pegawai).trigger('change');

                           var selected_jenjang_pendidikan = $("<option selected='selected'></option>").val(data.pendidikan_terakhir_id).text(data.nm_pendidikan);
                           $("#txtedit_jenjang_pendidikan").append(selected_jenjang_pendidikan).trigger('change');
                           $('#modal-edit-pegawai').modal('show');
                       }
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error);
                        var result = error.responseJSON;
                        if (result != null) {
                            var message = result.message;
                            if (Array.isArray(message)) {
                                $.map(message, function (value, index) {
                                    message = value + '</br>';
                                });
                            }
                        } else {
                            message = 'look like something when wrong';
                        }
                    } else {
                        message = 'look like something when wrong';
                    }

                    $('.cssload-container').hide();
                    Swal.fire({
                        title: 'Warning',
                        html: '<strong>' + message + '</strong>',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 3000
                    })

                }

            })
        }

       $('.tabel_show tbody').on('click', '#btn-delete-pegawai', function () {
        var url = '{{ url('destroy_data_pegawai') }}';
        var id = $(this).attr('data-id');
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var data_pegawai = {_token: CSRF_TOKEN, id: id};
        delete_master(url, data_pegawai);
        });

    });

</script>
@endsection

