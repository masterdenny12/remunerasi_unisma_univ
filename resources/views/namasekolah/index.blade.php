@extends('master')
@section('konten')
<div class="m-portlet m-portlet--mobile">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											<i class="m-menu__link-icon flaticon-list"></i>&nbsp Master Institusi
										</h3>
									</div>
								</div>
								<div class="m-portlet__head-tools">
									<ul class="m-portlet__nav">
										<li class="m-portlet__nav-item">
											<a href="#" data-toggle="modal" data-target="#CreateNamaSekolahModal" class="btn btn-success m-btn m-btn--icon m-btn--air">
												<span>
													<i class="la la-plus"></i>
													<span>Tambah Data</span>
												</span>
											</a>
										</li>
										<li class="m-portlet__nav-item"></li>
										<li class="m-portlet__nav-item">
											<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
												<a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
													<i class="la la-ellipsis-h m--font-brand"></i>
												</a>

											</div>
										</li>
									</ul>
								</div>
							</div>
							<div class="m-portlet__body">

								<!--begin: Datatable -->
								<table class="table table-striped- table-bordered table-hover tabel_show" >
									<thead>
										<tr>
                                            <th>No.</th>
                                            <th>Institusi</th>
                                            <th>Keterangan</th>
											<th>Update at</th>
											<th>Aksi</th>

										</tr>
									</thead>

								</table>
							</div>
						</div>


 <!-- Create Modal -->
 <div class="modal" id="CreateNamaSekolahModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Tambah Nama Sekolah</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                    <strong>Success!</strong>Product was added successfully.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="form-group">
                    <label for="Name">Nama Institusi:</label>
                    <input type="text" class="form-control" placeholder="Nama sekolah" name="nama_sekolah" id="tambah_nama_sekolah">
                </div>
                <div class="form-group">
                    <label for="Name">Keterangan:</label>
                    <input type="text" class="form-control" placeholder="keterangan" name="keterangan" id="tambah_keterangan">
                </div>

            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="SubmitCreateForm">Create</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

       {{--modal edit ikatan kerja--}}
       <div class="modal" id="modal-edit-nama-sekolah" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="form_edit_nama_sekolah" method="post">
                    <div class="modal-body">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Nama Institusi:</label>
                            <input type="hidden" name="id" class="form-control" id="txtedit_id">
                            <input type="text" placeholder="Institusi" name="nama_sekolah" class="form-control" id="txtedit_nama_sekolah" required>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Keterangan:</label>
                            <input type="text" placeholder="Keterangan" name="keterangan" class="form-control" id="txtedit_keterangan"
                                   >
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success btn-flat">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
    <script src="js_crud/index.js"></script>
<script>

    $(document).ready(function(){

        var table_ikatan_kerja = $('.tabel_show').DataTable({
            responsive:true,
            paging: true,
            info: true,
            searching: true,
            "aaSorting": [],
            "ordering": true,
            ajax: {
                url: '{{url("datatable_nama_sekolah")}}',
                dataSrc: 'result',
            },

        });

        $('#SubmitCreateForm').click(function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{ url('simpan_master_nama_sekolah') }}",
                method: 'post',
                data: {
                    nama_sekolah: $('#tambah_nama_sekolah').val(),
                    keterangan: $('#tambah_keterangan').val(),

                },
                success: function(result) {
                    if(result.errors) {
                        $('.alert-danger').html('');
                        $.each(result.errors, function(key, value) {
                            $('.alert-danger').show();
                            $('.alert-danger').append('<strong><li>'+value+'</li></strong>');
                        });
                    } else {
                        $('.alert-danger').hide();
                        $('.alert-success').show();
                        swal.fire({
                            title: "Information!",
                            html: '<strong>' + result.success + '</strong>',
                            type:'success',
                            button: false,
                            timer: 2000,
                        })
                        $('.tabel_show').DataTable().ajax.reload();
                            $('.alert-success').hide();
                            $('#CreateNamaSekolahModal').modal('hide');


                    }
                }
            });
        });

        $('#form_edit_nama_sekolah').submit(function (event) {
            $('#modal-edit-nama-sekolah').modal('hide');
            event.preventDefault();
            var url = '{{ url('update_master_nama_sekolah') }}';
            ajaxProcess(url, $(this).serialize())
        });

        function ajaxProcess(url, data) {
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                beforeSend: function () {
                    $('.cssload-container').show();
                },
                success: function (response) {
                    if (response.status == 'success') {
                        $('.cssload-container').hide();

                        $('.tabel_show').DataTable().ajax.reload();


                        Swal.fire({
                            title: 'Information',
                            html: '<strong>' + response.result + '</strong>',
                            type: 'success',
                            showConfirmButton: false,
                            timer: 3000
                        });
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error);
                        var result = error.responseJSON;
                        if (result != null) {
                            var message = result.message;
                            if (Array.isArray(message)) {
                                $.map(message, function (value, index) {
                                    message = value + '</br>';
                                });
                            }
                        } else {
                            message = 'look like something when wrong';
                        }
                    } else {
                        message = 'look like something when wrong';
                    }


                    $('#form_edit_nama_sekolah')[0].reset();
                    $('.cssload-container').hide();
                    Swal.fire({
                        title: 'Warning',
                        html: '<strong>' + message + '</strong>',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 3000
                    })

                }

            })
        };


        //show detail data
        modal_edit_nama_sekolah();

        function modal_edit_nama_sekolah() {
            $('#modal-edit-nama-sekolah').on('show.bs.modal', function (event) {
                // do something...
                var button = $(event.relatedTarget) // Button that triggered the modal
                var id = button.data('id');
                var nama_sekolah = button.data('nama_sekolah');
                var keterangan = button.data('keterangan');
                $('#txtedit_id').val(id);
                $('#txtedit_nama_sekolah').val(nama_sekolah);
                $('#txtedit_keterangan').val(keterangan);
            });
        }

       $('.tabel_show tbody').on('click', '#btn-delete-nama-sekolah', function () {
        var url = '{{ url('destroy_master_nama_sekolah') }}';
        var id = $(this).attr('data-id');
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var nama_sekolah = {_token: CSRF_TOKEN, id: id};
        delete_master(url, nama_sekolah);
        });

    });

</script>
@endsection

